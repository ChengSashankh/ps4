# CS3217 Problem Set 3

**Name:** Sashankh Chengavalli Kumar

**Matric No:** A0162363J

## Tips
1. CS3217's docs is at https://cs3217.netlify.com. Do visit the docs often, as
   it contains all things relevant to CS3217.
2. A Swiftlint configuration file is provided for you. It is recommended for you
   to use Swiftlint and follow this configuration. We opted in all rules and
   then slowly removed some rules we found unwieldy; as such, if you discover
   any rule that you think should be added/removed, do notify the teaching staff
   and we will consider changing it!

   In addition, keep in mind that, ultimately, this tool is only a guideline;
   some exceptions may be made as long as code quality is not compromised.
3. Do not burn out. Have fun!

## Game Rules and Features

This game is called Anatomy Park, and is inspired by an episode of Rick and Morty 
by the same name. This game has an element of skill and a good deal of luck too, as
it goes in life. 

In this game, the player must attempt to kill all the virus cells (pegs) before time runs 
out. 

- Game win: All pegs destroyed before time and attempts run out.
- Game lose: Pegs still remain. 

Game play:
- The ball will be launched a total of 3 times.
- For the first attempt, the ball will be launched in a direction of your choice, but with a 
fixed speed. This can be selected by clicking a point on the canvas, and then clicking start.
- Ball will be in play for 8 seconds, and can bounce off all surfaces. The virus cells hit are highlighted
and removed at the end of the run. 
- For the next 2 attempts, the ball will be launched in a random direction based on luck. 
- At the end of all attempts, if any virus cells remain, the game will end.
- When the game ends, a pop up shows the game status. 
- To play another game, go back to the home screen and return to the play screen.

Powerups:
- Space blast power up: For 5 seconds after a random Green virus cell is hit, the ball will
destroy anything within an extended radius (at the end of the run). The background also
tints green when this happens.
- Life line power up (modified spooky ball): For 7 seconds after a random Green virus cell 
is hit, the ball will automatically teleport to the top of the screen if it moved into the bottom
area. 

Other features:
- Scoring system: A game scorer tracks and displays the score on the screen.
- Progress messages: As the score increases, there are different messages shown on the 
screen according to the score at that time. 
- Timer:
    - There is a timer that runs for the game. Each run is restricted to 8 seconds.
    - Power up timer. Each power up also has a limited duration as mentioned above.
- Number of pegs remaining: The number of remaining pegs after each run is also updated
on the screen. 
- A background music features was being implemented, but it is not compeletely operational.


## Dev Guide
This game is divided into several modular components, and each is explained below
in the subsections. 

A game loop is responsible for updating the game at every frame, and is carried out
as shown in the diagram below. 

![Game Loop Sequence](https://github.com/cs3217-1920/2020-ps3-ChengSashankh/blob/documentation-update/GameLoop%20Sequence%20Diagram.png?raw=true)

To achieve separation of concerns in this project, we divided the code base into: 
- `physics-engine`: Responsible for calculation of physics entites and formulae.
- `game-state`: Responsible for maintaining and updating the model (encapsulated)
- `game-loop`: Encapsulates the game loop execution.
- `game-interface`: Interfaces communication between the ViewController and GameLoop.
- `game-renderer`: Game renderer triggers update of the UI with changes in game state.

The diagram below shows the interaction between these components.

![ Interaction between the components ](https://raw.githubusercontent.com/ChengSashankh/images/master/Interaction%20Diagram.png)

The Physics Engine is organized as shown in the class diagram below:

![ Physics Engine Class diagram ](https://raw.githubusercontent.com/ChengSashankh/images/master/PhysicsEngine%20Class%20Diagram.png)

The GameState is organized as shown in the diagram below:

![Game State Class Diagram](https://raw.githubusercontent.com/ChengSashankh/images/master/GameState%20Class%20Diagram.png)

## User Instructions

1. In order to launch the ball, select a point on the canvas to indicate the direction of launch 
and then press the start button. 

2. The start button does not trigger any action if a point is not selected on the canvas first.

## Tests

In order to test this game, we must test can test the components separately to allow 
for a significant degree of confidence. 

### Physics Engine

The physics engine is tested using a combination of unit tests (implemented) and manual
testing. Both are detailed in the section below.

Positive unit test cases are implemented, while the negative cases are discussed here:
- `Particle.swift`
    - `init` method
        - When called with dimension, bounding method and payload, the values should be set accordingly, and set movable to true, and position velocity acceleration to zero vectors.
        - When called with position, velocity and acceleration, should set these values into the respective properties
        - When called with velocity, position or acceleration vector of incorrect dimension, should throw `VectorError.invalidDimension` error.
    - `setRadiusBounds` method
        - When called, should set the given radius into the `boundingRadius` property.
    - `setPosition` method
        - When called, should set the given position vector into the `position` property.
    - `updateFor` method
        - When called, should update velocity and position after the given number of seconds. 
    - `updateVelocityAfter` method
        - When called, should update velocity after the given number of seconds.
    - `updatePositionAfter` method
        - When called, should update position after the given number of seconds.
- `Vector.swift`
    -`init` method
        - When called with dimension, should set into the property.
        - When called with values, should set into the property.
        - When called with vector, should copy properties into self.
    - `norm` method
        - Should return the norm of the vector (defined as sqrt(sum of squares of components))
        - If vector has 0 dimensions, should return 0
    - `scale` method
        - Should take in a constant and multiply all components of vector by the constant. 
    - `distanceTo` method
        - Should return Euclidean distance between points represented by both vectors
        - Should throw `VectorError.invalidDimension` error if vectors are not of same dimension.
    - `add` method
        - Should update values with component wise sum of components from both vectors.
        - If other vector contains fewer components, should throw error.
        - If other vector contains more components, should use first `dimension` number of components.
    - `subtract` method
        - Should update values with component wise difference of components from both vectors.
        - If other vector contains fewer components, should throw error.
        - If other vector contains more components, should use first `dimension` number of components.
    - `multiplyComponents` method
        - Should update values with component wise multiplication of components from both vectors.
        - If other vector contains fewer components, should throw error.
        - If other vector contains more components, should use first `dimension` number of components.
    - `dot` method
        - Should return dot product value of both vectors.
        - If other vector contains fewer components, should throw error.
        - If other vector contains more components, should use first `dimension` number of components.
    - `cosOfAngleWith` method
        - Should return cos of angle between the vectors
    - `sinOfAngleWith` method
        - Should return sin of angle between the vectors
- `PhysicsEngine.swift`
    - `setBounds` method
        - Should set the bounds property with the given value
    - `addParticleToWorld` method
        - Should add the particle to the world in `ParticleWatcher`
    - `removeParticleFromWorld` method
        - Should add the particle to the world in `ParticleWatcher` 
    - `updateAfter` method
        - Should trigger update method of each particle in scene
    - `getCollisions` method
        - Should return the collisions between particles when they exist.
    - `handleCollisions` method
        - Should set new velocities of each particle in collision after the collision.
    - `getCollisionResult` method
        - Should update the velocities of the particles in this particular collision according to
        the rules

The rest of the classes (e.g., ParticleWatcher) can be covered by these tests and their operations generally 
maintain correspondance with one of the above mentioned class operations.

Additionally, we may check the following.

The role of the physics engine in this game is to simulate the movement of the ball 
under the influence of gravity and an initial velocity, and to simulate collisions with other
bodies. This gives us three testable parts to this task. We can test all these by launching
the ball during gameplay and observing its movement. 

1. Does the ball appear to move well under gravity?

This can be checked by looking at how the ball moves around and we can quickly tell that
the ball is moving under a constant downward acceleration (like gravity). The acceleration is
adjusted to look similar to real life. 

2. When the ball collides with a wall/other rectangular surface, does it bounce off naturally?

As the ball hits one of the walls, it bounces off as it should in an elastic collision. The ball 
bounces away in a similar angle in the opposite direction, as we would expect. 

3. When the ball collides with a peg, does the ball bounce off naturally?

The bouncing off from pegs is slightly less intuitive, but it was implemented similar to rectangular
surfaces for simplicity. 

### Game Interface

This part is relatively easy to test. We can follow these steps to ensure that it behaves correctly 
to ensure that the integration of components is working correctly. 

1. Click on the start button. Does the ball launch, or any other action take place before you 
select a valid launch direction? Ideally, this should not happen.

2. Click on any part of the canvas. Does the black rectangular marker appear there to indicate
that the direction has been recorded in the app? 

3. Now click the start button. Does the ball launch with an initial velocity in that general direction?

### Game Loop

The game loop is the most integral part of the game. The failure of the game loop has some very
clear signs. Any of the following signs can indicate such a failure.

1. Appearance or disappearance of objects on the screen unexpectedly.
2. Lags and delays between frames inconsistently can indicate that the processing is taking 
longer than the frame rate, and that the game loop is not adapting correctly. 
3. User inputs are not recorded. The disappearance of an aiming marker, or not showing one on
click can indicate that information from one loop is not completely process/passed on to the next.

### Game State

In this case, we are loading a sample level for the game engine. Hence, we will focus the testing 
on the manipulation of game state during game play alone. 

GameState should ideally query the physics engine correctly, and update the state to match. If you
see any of the following errors, it is likely an issue with the GameState class. 

1. Ball collides with Peg without the highlighting taking place.
2. Game rules are not heeded by the ball (stop on hitting bottom, etc.)
3. Collisions are taking place between objects that don't appear on screen, or,
4. Collisions don't take place with the objects on the screen. 

(3) and (4) both indicate that the GameState class has not correctly updated the physics engine about
the position of various game objects. 

Additionally, we can explore the following unit test cases for `GameState.swift`:
    - `init` method should set the center point property received either as a Vector or 
    as a CGPoint. If nil, should initialize with zero vectors.
    - `initializeStateWithLevel` method should add all pegs from Level into dictionary 
    and update physics engine for each.
    - `update` method should 
        - stop on game end
        - update based on input given in the last frame (checks `updateBasedOnInput`)
        - update based on physics engine update method (checks `updateBasedOnMotion`)
        - recalculate game ended status 
    - `launchBallAtAngle` should set initial velocity of ball at launch based on direction of 
    clickPoint from centerPoint, of fixed magnitude 
    - `getInitialVelocityFromDirection` should return Vector of direction from centerPoint 
    to clickPoint. Vector should be of magnitude 300

In order to test the model, we can test the following classes.

- `LevelEntityLocation.swift`
    - Should set xLocation and yLocation correctly on init
    - Should initialize as zeros when incorrect dimension is supplied.
- `Peg.swift`
    - Should set properties correctly on init
    - Should set location provided by overwrite when `setLocation` method is called
- `Level.swift`
    - Should appropriate properties when `setName` is called
    - Should return straight line distance when two locations are provided to `getPegDistance`
    - `checkMinimumDistanceConstraint` should return true unless given location
    is within `minimumPegDistance` of an existing object
    - `pegCanBeAdded` should return true only if its color is valid, it doesn't overlap
    with an existing peg
    - `addPeg` should return `nil` if it cannot be added according to constraints. Else, it should add the peg to pegList, its location to pegLocation and update lastUpdated. The peg object should be returned.
    -`removePeg` should return `nil` if the provided peg does not exist. Else, it should remove 
    the peg from pegList, remove its location from pegLocations and update
    lastUpdated. The peg should be returned.
    - `removeAll` should remove all pegs, peg locations and set lastUpdated.

## Design Tradeoffs 

### KD-Tree vs Brute Force

In order to select the particles that may be colliding, it is important to take 
some effort to make the process efficient. For example, some physics engines
may choose to implement a pre-selection of particles so that we don't have to 
check for collisions between all particles at O(n^2) cost. 

One way to do this would be using a KD-Tree. A KD-Tree can provide elements
within a radius of an object in sublinear time. This allows for more efficient checks
for collisions.

In this case, at a frame rate of 60 FPS, there is about 16 ms for the collision checks 
and other operations at every game loop. With <100 pegs on the screen at once, the
checks can be comfortably completed in less than the given time. I decided not to 
implement a KD-Tree for this task as that would involve a significant amount of testing
and implementation work.

### Software Architecture

Given the significant complexity of the software for this problem set, I was looking at 
the way I could reduce the amount of the debugging work needed to be done. To this
end, I had to select between a 3 component structure (rendering/game engine/physics 
engine), or the more fragmented, but clear structure that I ended up using. 

I chose to split the game into smaller modules to enable greater testability (at a later 
stage) and make it easier to debug. The current structure makes the role of each segment
more apparent, and this is important to ensure later development is easy.


### Method of collision checking

Initially, I was considering modelling all the objects as circles or rectangles, and using  
a general physics engine to calculate the collisons (even for out of bounds, etc.). For example,
the four sides of the canvas would be considered rectangular surfaces. 

This method required me to write methods to check for collisions between rectangular and circular
bodies, circular-circular and rectangular-rectangular collisions. This was becoming much 
more complicated than I expected, and I decided to do away with these and just implement
circular bodies. Collision with edges of the canvas was written separately. This made the code
simpler, but a little less general. Given the scope of PS3, this was an acceptable trade-off for me.
