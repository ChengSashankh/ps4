//
//  ParticleTests.swift
//  peggle-physics-engineTests
//
//  Created by Sashankh Chengavalli Kumar on 17.02.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import XCTest
@testable import peggle

class ParticleTest: XCTestCase {
    var ball = Ball(ballLocation: LevelEntityLocation(xLocation: 0.0, yLocation: 0.0))

    func testUpdateAfterTimeT_shouldUpdateCorrectly() {
        do {
            let particle = try Particle<Ball>(
                dim: 2,
                mass: 10,
                initialPosition: Vector(dimension: 2),
                initialVelocity: Vector(dimension: 2),
                initialAcceleration: Vector(values: [0.0, 9.8]),
                movable: true,
                payload: ball
            )

            _ = try particle.updateFor(seconds: 1)

            XCTAssertEqual(particle.acceleration, Vector(values: [0.0, 9.8]))
            XCTAssertEqual(particle.velocity, Vector(values: [0.0, 9.8]))
            XCTAssertEqual(particle.position, Vector(values: [0.0, 4.9]))
        } catch {
            XCTFail()
        }
    }
}
