//
//  PhysicsEngineTests.swift
//  peggle-tests
//
//  Created by Sashankh Chengavalli Kumar on 22.02.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import XCTest
@testable import peggle

class PhysicsEngineTests: XCTestCase {
    var ball = Ball(ballLocation: LevelEntityLocation(xLocation: 0.0, yLocation: 0.0))
    var physicsEngine = PhysicsEngine()

    func testInitialWorldEmpty_shouldBeEmpty() {
        let particles = physicsEngine.particleWatcher.particlesInScene
        XCTAssertEqual(particles.count, 0)
    }

    func testAddParticle_shouldHaveParticle() {
        let particleToAdd = Particle<PhysicsEngineTrackableObject>(
            dim: 2, boundingMethod: BoundingType.RADIUS, payload: ball
        )

        physicsEngine.addParticleToWorld(
            particle: particleToAdd
        )

        let particles = physicsEngine.particleWatcher.particlesInScene
        XCTAssertEqual(particles.count, 1)
        XCTAssertEqual(particles[0], particleToAdd)
    }

    func testRemoveParticle_shouldNotHaveParticle() {
        let particleToRemove = Particle<PhysicsEngineTrackableObject>(
            dim: 2, boundingMethod: BoundingType.RADIUS, payload: ball
        )

        physicsEngine.removeParticleFromWorld(
            particle: particleToRemove
        )

        let particles = physicsEngine.particleWatcher.particlesInScene
        XCTAssertEqual(particles.count, 0)
    }

    func testSetBounds_shouldSetBoundsCorrectly() {
        let bounds = [
            Vector(values: [0.0, 0.0]),
            Vector(values: [2000.0, 0.0]),
            Vector(values: [0.0, 2000.0]),
            Vector(values: [2000.0, 2000.0])
        ]

        physicsEngine.setBounds(bounds: bounds)

        XCTAssertEqual(physicsEngine.particleWatcher.bounds, bounds)
    }

    func testUpdateParticle_shouldUpdateCorrectly() {
        let bounds = [
            Vector(values: [0.0, 0.0]),
            Vector(values: [2000.0, 0.0]),
            Vector(values: [0.0, 2000.0]),
            Vector(values: [2000.0, 2000.0])
        ]

        physicsEngine.setBounds(bounds: bounds)

        do {
            let particleToAdd = Particle<PhysicsEngineTrackableObject>(
                dim: 2,
                mass: 10,
                initialPosition: Vector(values: [500.0, 500.0]),
                initialVelocity: Vector(dimension: 2),
                initialAcceleration: Vector(values: [0.0, 9.8]),
                movable: true,
                payload: ball
            )

            physicsEngine.addParticleToWorld(particle: particleToAdd)

            _ = try physicsEngine.updateAfter(seconds: 1)

            XCTAssertEqual(particleToAdd.acceleration.values, [0.0, 9.8])
            XCTAssertEqual(particleToAdd.velocity.values, [0.0, 9.8])
            XCTAssertEqual(particleToAdd.position.values, [500.0, 504.9])
        } catch {
            XCTFail()
        }
    }

    func testCollisionDetection_shouldDetectCollision() {
        let bounds = [
            Vector(values: [0.0, 0.0]),
            Vector(values: [2000.0, 0.0]),
            Vector(values: [0.0, 2000.0]),
            Vector(values: [2000.0, 2000.0])
        ]

        physicsEngine.setBounds(bounds: bounds)

        do {
            let firstParticleToAdd = try Particle<PhysicsEngineTrackableObject>(
                dim: 2,
                mass: 100,
                initialPosition: Vector(values: [499.9, 503.0]),
                initialVelocity: Vector(dimension: 2),
                initialAcceleration: Vector(values: [0.0, 9.8]),
                movable: true,
                payload: ball
            )

            let secondParticleToAdd = try Particle<PhysicsEngineTrackableObject>(
                dim: 2,
                mass: 10,
                initialPosition: Vector(values: [500.9, 502.0]),
                initialVelocity: Vector(dimension: 2),
                initialAcceleration: Vector(values: [0.0, 9.8]),
                movable: true,
                payload: ball
            )

            physicsEngine.addParticleToWorld(particle: firstParticleToAdd)
            physicsEngine.addParticleToWorld(particle: secondParticleToAdd)

            let collisions = try physicsEngine.getCollisions()

            XCTAssertEqual(collisions.count, 1)
            XCTAssertTrue(collisions.contains(Set<Particle<PhysicsEngineTrackableObject>>([firstParticleToAdd, secondParticleToAdd])))
        } catch {
            XCTFail()
        }
    }

    func testHandleCollisions_shouldCalculateCorrectResult() {
        let bounds = [
            Vector(values: [0.0, 0.0]),
            Vector(values: [2000.0, 0.0]),
            Vector(values: [0.0, 2000.0]),
            Vector(values: [2000.0, 2000.0])
        ]

        physicsEngine.setBounds(bounds: bounds)

        do {
            let firstParticleToAdd = try Particle<PhysicsEngineTrackableObject>(
                dim: 2,
                mass: 100,
                initialPosition: Vector(values: [499.9, 503.0]),
                initialVelocity: Vector(values: [5.0, 5.0]),
                initialAcceleration: Vector(values: [0.0, 9.8]),
                movable: true,
                payload: ball
            )

            let secondParticleToAdd = try Particle<PhysicsEngineTrackableObject>(
                dim: 2,
                mass: 10,
                initialPosition: Vector(values: [500.9, 502.0]),
                initialVelocity: Vector(values: [10.0, 10.0]),
                initialAcceleration: Vector(values: [0.0, 9.8]),
                movable: true,
                payload: ball
            )

            physicsEngine.addParticleToWorld(particle: firstParticleToAdd)
            physicsEngine.addParticleToWorld(particle: secondParticleToAdd)

            let collisions = try physicsEngine.getCollisions()
            physicsEngine.handleCollisions(collisions: collisions)

            let particles = physicsEngine.particleWatcher.particlesInScene
            XCTAssertTrue(particles[0].velocity.values == [-4.0, -4.0])
            XCTAssertTrue(particles[1].velocity.values == [-8.0, -8.0])
        } catch {
            XCTFail()
        }
    }
    
}
