//
//  VectorTests.swift
//  peggle-tests
//
//  Created by Sashankh Chengavalli Kumar on 22.02.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import XCTest
@testable import peggle

class VectorTests: XCTestCase {

    func testVectorNorm_shouldReturnCorrectValue() {
        let vector = Vector(values: [3.0, 4.0])
        XCTAssertEqual(vector.norm(), 5.0)
    }

    func testVectorDot_shouldReturnCorrectValue() {
        let firstVector = Vector(values: [3.0, 4.0])
        let secondVector = Vector(values: [5.0, 7.0])

        XCTAssertEqual(firstVector.dot(otherVector: secondVector), 43.0)
        XCTAssertEqual(secondVector.dot(otherVector: firstVector), 43.0)
    }

    func testVectorCosValue_shouldReturnCorrectValue() {
        var firstVector = Vector(values: [0.0, 1.0])
        var secondVector = Vector(values: [1.0, 0.0])

        XCTAssertEqual(firstVector.cosOfAngleWith(otherVector: secondVector), 0.0)
        XCTAssertEqual(secondVector.cosOfAngleWith(otherVector: firstVector), 0.0)

        XCTAssertEqual(firstVector.cosOfAngleWith(otherVector: firstVector), 1.0)

        firstVector = Vector(values: [1.0, 1.0])
        secondVector = Vector(values: [0.0, 1.0])

        XCTAssertEqual(firstVector.cosOfAngleWith(otherVector: secondVector), 1.0/sqrt(2))
        XCTAssertEqual(secondVector.cosOfAngleWith(otherVector: firstVector), 1.0/sqrt(2))
    }

    func testVectorAdd_shouldReturnCorrectValue() {
        let firstVector = Vector(values: [1.1, 2.7])
        let secondVector = Vector(values: [1.5, 3.0])

        firstVector.add(otherVector: secondVector)

        XCTAssertEqual(firstVector.values, [2.6, 5.7])
    }

    func testVectorSubtract_shouldReturnCorrectValue() {
        let firstVector = Vector(values: [2.0, 3.0])
        let secondVector = Vector(values: [1.5, 3.0])

        do {
            try firstVector.subtract(otherVector: secondVector)
        } catch {
            XCTFail()
        }

        XCTAssertEqual(firstVector.values, [0.5, 0.0])
    }

    func testVectorMultiplyComponents_shouldReturnCorrectValue() {
        let firstVector = Vector(values: [2.0, 3.0])
        let secondVector = Vector(values: [1.5, 3.0])

        do {
            try firstVector.multiplyComponents(otherVector: secondVector)
        } catch {
            XCTFail()
        }

        XCTAssertEqual(firstVector.values, [3.0, 9.0])
    }

    func testDistanceTo_shouldReturnCorrectValue() {
        let firstPoint = Vector(values: [6.5, 15.0])
        let secondPoint = Vector(values: [1.5, 3.0])

        do {
            let distance = try firstPoint.distanceTo(otherVector: secondPoint)
            XCTAssertEqual(distance, 13.0)
        } catch {
            XCTFail()
        }
    }

    func testVectorScaling_shouldReturnCorrectValue() {
        let firstVector = Vector(values: [6.5, 15.0])

        firstVector.scale(constant: 10.0)
        XCTAssertEqual(firstVector.values, [65.0, 150.0])
    }

    func testDistanceToLine_shouldReturnCorrectValue() {
        let point = Vector(values: [5, 5])
        let line = Line(startPoint: Vector(values: [-5, 5]), endPoint: Vector(values: [5, -5]))

        XCTAssertEqual(point.distanceToLine(line: line), sqrt(50), accuracy: 0.001)
    }
}
