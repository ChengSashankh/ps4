//
//  BackgroundMusicHelper.swift
//  peggle
//
//  Created by Sashankh Chengavalli Kumar on 01.03.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import AVFoundation

class BackgroundMusicHelper {
    static let backgroundMusicHelperInstance = BackgroundMusicHelper()
    var audioPlayer: AVAudioPlayer?

    // TODO: Put the mp3 file in the correct path
    func playBackgroundMusic(fileName: String?) {
        let musicFileName = fileName ?? "background"

        let aSound = NSURL(
            fileURLWithPath: Bundle.main.path(forResource: musicFileName, ofType: "mp3")!
        )
        do {
            audioPlayer = try AVAudioPlayer(contentsOf:aSound as URL)
            audioPlayer!.numberOfLoops = -1
            audioPlayer!.prepareToPlay()
            audioPlayer!.play()
        }
        catch {
            print("Cannot play the file")
        }
    }
}

