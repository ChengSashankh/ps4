//
//  Ball.swift
//  peggle-physics-engine
//
//  Created by Sashankh Chengavalli Kumar on 15.02.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import Foundation

class Ball: PhysicsEngineTrackableObject {
    var ballLocation: LevelEntityLocation

    private enum CodingKeys: String, CodingKey {
        case ballLocation
    }

    init(ballLocation: LevelEntityLocation) {
        self.ballLocation = ballLocation
        super.init(id: 0)
    }
    
    static func == (lhs: Ball, rhs: Ball) -> Bool {
        return
            lhs.ballLocation == rhs.ballLocation
    }

    override func hash(into hasher: inout Hasher) {
        hasher.combine(ballLocation)
    }

    override func setLocation(vector: Vector) throws {
        ballLocation = LevelEntityLocation(vector: vector)
    }
}
