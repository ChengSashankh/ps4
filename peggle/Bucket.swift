//
//  Bucket.swift
//  peggle
//
//  Created by Sashankh Chengavalli Kumar on 02.03.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import Foundation

class Bucket {
    var position: LevelEntityLocation
    var vertices: [LevelEntityLocation]

    init(position: LevelEntityLocation, vertices: [LevelEntityLocation]) {
        self.position = position
        self.vertices = vertices
    }
}
