//
//  CollisionHandler.swift
//  peggle
//
//  Created by Sashankh Chengavalli Kumar on 29.02.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import Foundation

class CollisionHandler<T: PhysicsEngineTrackableObject> {
    // Precondition: Meant for reflection of one static and movable particle
    static func reflectCircleOffCircle(moving: Particle<T>, fixed: Particle<T>) {
        assert(moving.movable)
        assert(!fixed.movable)

        let lineJoiningCenters = moving.getLineJoiningCenters(from: fixed)
        let tangentDirection = lineJoiningCenters.getPerpendicularDirection()

        reflectParticleOffSurface(particle: moving, surface: tangentDirection)
    }

    // Precondition: Meant for reflection from static triangle particle
    static func reflectCircleOffTriangle(circle: Particle<T>, triangle: Particle<T>) {
        assert(!triangle.movable)
        assert(circle.movable)

        let nearestSide = triangle.getNearestSideToPoint(point: circle.position)
        let nearestSideDirection = Vector(vector: nearestSide.p2)
        nearestSideDirection.subtract(otherVector: nearestSide.p1)

        reflectParticleOffSurface(particle: circle, surface: nearestSideDirection)
    }

    static func reflectParticleOffSurface(particle: Particle<T>, surface: Vector) {
        let originalMagnitude = particle.velocity.norm()

        let componentAlong = surface.getComponentAlong(otherVector: particle.velocity)
        let componentPerpendicular = particle.velocity.getComponentPerpendicular(otherVector: surface)

        componentPerpendicular.scale(constant: -1)
        componentAlong.add(otherVector: componentPerpendicular)

        componentAlong.scale(constant: originalMagnitude * 0.97/componentAlong.norm())

        particle.velocity = componentAlong
    }
}
