//
//  CreateLevelViewController.swift
//  peggles
//
//  Created by Sashankh Chengavalli Kumar on 28.01.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import UIKit

class CreateLevelViewController: UIViewController, UIGestureRecognizerDelegate {
    var levelNames = [URL]()

    var currentPegColor: String = "Red"
    var currentPegShape: String = "Circle"
    var lastTouchLocation: CGPoint = CGPoint()
    var lastPanStartLocation: CGPoint?
    var lastPanEndLocation: CGPoint?
    var selectedPeg: UIImageView?

    var levelEditor: LevelEditor = LevelEditor()
    var componentToPeg: [UIImageView: Peg] = [UIImageView: Peg]()

    let pegWidth = 20
    let pegHeight = 20

    @IBOutlet weak var uiImageView: UIImageView!
    @IBOutlet weak var segmentedColorPicker: UISegmentedControl!
    @IBOutlet weak var levelNameTextField: UITextField!
    @IBOutlet weak var segmentedShapePicker: UISegmentedControl!

    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet var resetButton: UIView!
    @IBOutlet weak var loadButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var setButton: UIButton!
    @IBOutlet weak var backButton: UIButton!

    required init(coder decoder: NSCoder) {
        super.init(coder: decoder)!
    }

    override func viewDidLoad() {
        super.viewDidLoad()

//        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
//        backgroundImage.image = UIImage(named: "homepage.png")
//        backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
//        self.view.insertSubview(backgroundImage, at: 0)
//
//        deleteButton.backgroundColor = .black
//        deleteButton.layer.cornerRadius = 5
//        resetButton.backgroundColor = .black
//        resetButton.layer.cornerRadius = 5
//        loadButton.backgroundColor = .black
//        loadButton.layer.cornerRadius = 5
//        saveButton.backgroundColor = .black
//        saveButton.layer.cornerRadius = 5
//        setButton.backgroundColor = .black
//        setButton.layer.cornerRadius = 5
//        backButton.backgroundColor = .black
//        backButton.layer.cornerRadius = 5
//        segmentedColorPicker.backgroundColor = .white
//        segmentedShapePicker.backgroundColor = .white

        let width = Double(self.view.frame.width) - 40.0
        let height = (600.0/800.0) * width

        uiImageView.frame = CGRect(
            origin: CGPoint(x: 20, y: 250),
            size: CGSize(width: width, height: height)
        )

        uiImageView.image = UIImage(named: "background-anatomy.png")!

        let singleTap = UITapGestureRecognizer(
            target: self, action: #selector(onAddPegTap))
        uiImageView.addGestureRecognizer(singleTap)

//        self.view.addSubview(uiImageView)
//        self.view.bringSubviewToFront(uiImageView)
    }

    // UI action handlers

    @IBAction func deleteButtonClickHandler(_ sender: Any) {
        // Delete peg only if it is selected
        if !deletePeg(pegUiComponent: selectedPeg) {
            showToastMessage(
                message: "Click on a peg first, and then click delete to remove it!",
                seconds: 3.0,
                mode: "error"
            )
        }
    }

    @IBAction func resetButtonClickHandler(_ sender: UIButton) {
        levelEditor.removeAllPegs()
        levelNameTextField.text = nil

        for pegImageView in componentToPeg.keys {
            _ = levelEditor.removePeg(pegToRemove: componentToPeg[pegImageView]!)
            pegImageView.removeFromSuperview()
        }
    }

    @IBAction func loadButtonClickHandler(_ sender: UIButton) {
        levelNames = levelEditor.listLevelsToLoad()
    }

    @IBAction func saveButtonClickHandler(_ sender: UIButton) {
        if let levelName = levelEditor.getLevelName() {
            levelEditor.saveLevelToFile(fileName: levelName)
            showToastMessage(message: "Saved!", seconds: 1.5, mode: "info")
        } else {
            showToastMessage(message: "Enter valid level name to save!", seconds: 1.5, mode: "error")
        }
    }

    @IBAction func shapeChangeButtonClickHandler(_ sender: Any) {
        switch segmentedShapePicker.selectedSegmentIndex {
            case 0: currentPegShape = "Circle"
            case 1: currentPegShape = "Triangle"
            default: break
        }
    }

    @IBAction func colorChangeButtonClickHandler(_ sender: Any) {
        switch segmentedColorPicker.selectedSegmentIndex {
            case 0: currentPegColor = "Red"
            case 1: currentPegColor = "Blue"
            case 2: currentPegColor = "Green"
            default: break
        }
    }

    @IBAction func setNameButtonClickHandler(_ sender: Any) {
        if let levelName = levelNameTextField.text {
            levelEditor.setLevelName(newName: levelName)
        }
    }

    @IBAction func onAddPegTap(_ sender: Any) {
        let adjustedPegLocation = getTopLeftCornerPoint(centerPoint: lastTouchLocation)
        addPegAtLocation(touchLocation: adjustedPegLocation)
    }

    @IBAction func onLevelNameUpdated(_ sender: UITextField) {
        if let levelName = levelNameTextField.text {
            levelEditor.setLevelName(newName: levelName)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is LoadFileViewController {
            let destinationController = segue.destination as? LoadFileViewController
            destinationController?.levelNames = levelEditor.listLevelsToLoad()
            destinationController?.createLevelViewController = self
        }
    }

    @objc func selectPeg(sender: UITapGestureRecognizer) {
        guard let view = sender.view as? UIImageView else {
            return
        }
        self.selectedPeg = view
    }

    @objc func dragPeg(sender: UITapGestureRecognizer) {
        guard let view = sender.view as? UIImageView else {
            return
        }
        let movedPeg = view

        if sender.state == UIGestureRecognizer.State.began {
            lastPanStartLocation = sender.location(in: uiImageView)
        } else if sender.state == UIGestureRecognizer.State.ended {
            lastPanEndLocation = sender.location(in: uiImageView)
        }

        performMoveIfPossible(pegUiComponent: movedPeg)
    }

    @objc func longPressPeg(sender: UITapGestureRecognizer) {
        guard let view = sender.view as? UIImageView else {
            return
        }
        let pegToDelete = view
        _ = deletePeg(pegUiComponent: pegToDelete)
    }

    // Get and store latest touch location
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = event?.allTouches?.first {
            let loc: CGPoint = touch.location(in: touch.view)
            lastTouchLocation = loc
        }
    }

    func performMoveIfPossible(pegUiComponent: UIImageView) {
        if lastPanStartLocation != nil && lastPanEndLocation != nil {
            let toLocation = CGPoint(x: lastPanEndLocation!.x, y: lastPanEndLocation!.y)
            let adjustedNewOrigin = getTopLeftCornerPoint(centerPoint: toLocation)

            let wasMoved = movePeg(
                peg: componentToPeg[pegUiComponent],
                destinationPoint: toLocation
            )

            if wasMoved {
                pegUiComponent.removeFromSuperview()

                pegUiComponent.frame =
                    CGRect(
                        x: Double(adjustedNewOrigin.x),
                        y: Double(adjustedNewOrigin.y),
                        width: Double(pegWidth),
                        height: Double(pegHeight)
                    )

                self.uiImageView.addSubview(pegUiComponent)
                self.uiImageView.bringSubviewToFront(pegUiComponent)
            }

            lastPanStartLocation = nil
            lastPanEndLocation = nil
        }
    }

    // To show toast messages (error/info) for specified number of seconds
    func showToastMessage(message: String, seconds: Double, mode: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.alpha = 0.5
        alert.view.layer.cornerRadius = 15

        if mode == "error"{
            alert.view.backgroundColor = .red
        } else {
            alert.view.backgroundColor = .white
        }

        self.present(alert, animated: true)

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            alert.dismiss(animated: true)
        }
    }

    func deletePeg(pegUiComponent: UIImageView?) -> Bool {
        if pegUiComponent != nil && componentToPeg.keys.contains(pegUiComponent!) {
            UIView.transition(
                with: self.view, duration: 0.30, options: [.transitionCrossDissolve], animations: {
              pegUiComponent!.removeFromSuperview()
            }, completion: nil)
            
            if let peg = componentToPeg[pegUiComponent!] {
                _ = levelEditor.removePeg(pegToRemove: peg)
            }
            componentToPeg.removeValue(forKey: pegUiComponent!)
            return true
        }
        return false
    }

    func loadLevel(fileName: String) {
        // Clear the existing level from screen
        for pegImageView in componentToPeg.keys {
            _ = levelEditor.removePeg(pegToRemove: componentToPeg[pegImageView]!)
            pegImageView.removeFromSuperview()
        }

        levelNameTextField.text = nil

        // Load new level object
        levelEditor.loadFromFile(fileName: fileName)

        // Create new datastructures
        self.selectedPeg = nil
        self.componentToPeg = [UIImageView: Peg]()
        levelNameTextField.text = levelEditor.getLevelName()

        for peg in levelEditor.getLevel().pegList {
            let pegUIView = createPegUIComponent(
                touchLocation: CGPoint(x: peg.pegLocation.xLocation, y: peg.pegLocation.yLocation),
                pegColor: peg.pegColor,
                pegShape: peg.pegShape
            )
            self.componentToPeg[pegUIView] = peg
            self.uiImageView.addSubview(pegUIView)
            self.uiImageView.bringSubviewToFront(pegUIView)
        }

    }

    // This touch location is relative to the window
    func addPegAtLocation(touchLocation: CGPoint) {
        // Create a peg of the current color and location
        let pegToAdd = createPeg(
            pegColor: currentPegColor,
            pegPoint: touchLocation,
            pegShape: currentPegShape
        )

        // Add as subview on the uiImageView component only if successfully added
        if levelEditor.addPeg(pegToAdd: pegToAdd) {
            let pegImageView = createPegUIComponent(
                touchLocation: touchLocation,
                pegColor: currentPegColor,
                pegShape: currentPegShape
            )
            self.uiImageView.addSubview(pegImageView)
            self.uiImageView.bringSubviewToFront(pegImageView)
            componentToPeg[pegImageView] = pegToAdd
        }
    }

    func getTopLeftCornerPoint(centerPoint: CGPoint) -> CGPoint {
        return CGPoint(
            x: Double(centerPoint.x) - Double(pegWidth / 2),
            y: Double(centerPoint.y) - Double(pegHeight / 2)
        )
    }

    func createPegUIComponent(touchLocation: CGPoint, pegColor: String, pegShape: String) -> UIImageView {

        let pegImageView = UIImageView(
            frame: CGRect(
                x: Double(touchLocation.x),
                y: Double(touchLocation.y),
                width: Double(pegWidth),
                height: Double(pegHeight)
            )
        )

        switch [pegColor, pegShape] {
            case ["Blue", "Circle"]: pegImageView.image = UIImage(named: "peg-blue.png")!
            case ["Blue", "Triangle"]: pegImageView.image = UIImage(named: "peg-blue-triangle.png")!
            case ["Red", "Circle"]: pegImageView.image = UIImage(named: "peg-red.png")!
            case ["Red", "Triangle"]: pegImageView.image = UIImage(named: "peg-red-triangle.png")!
            case ["Green", "Circle"]: pegImageView.image = UIImage(named: "peg-green.png")!
            case ["Green", "Triangle"]: pegImageView.image = UIImage(named: "peg-green-triangle.png")!
            default: print ("Peg type not recognised")
        }

        pegImageView.isUserInteractionEnabled = true

        let singleTap = UITapGestureRecognizer(
            target: self, action: #selector(selectPeg))
        pegImageView.addGestureRecognizer(singleTap)

        let panGesture = UIPanGestureRecognizer(
            target: self, action: #selector(dragPeg))
        pegImageView.addGestureRecognizer(panGesture)
        panGesture.delegate = self

        let longPressGesture = UILongPressGestureRecognizer(
            target: self, action: #selector(longPressPeg)
        )
        pegImageView.addGestureRecognizer(longPressGesture)

        self.view.addSubview(pegImageView)
        return pegImageView
    }

    func createPeg(pegColor: String, pegPoint: CGPoint, pegShape: String) -> Peg {
        let pegLocation = LevelEntityLocation(
            xLocation: Double(pegPoint.x),
            yLocation: Double(pegPoint.y)
        )

        return Peg(
            pegLocation: pegLocation,
            pegColor: pegColor,
            pegShape: pegShape
        )
    }

    func movePeg(peg: Peg?, destinationPoint: CGPoint) -> Bool {
        if peg == nil {
            return false
        }

        let newPegLocation = LevelEntityLocation(
            xLocation: Double(destinationPoint.x),
            yLocation: Double(destinationPoint.y))

        levelEditor.updatePegLocation(pegToUpdate: peg!, newLocation: newPegLocation)
        return true
    }

    func getLevelNamesFromUrls(jsonFiles: [URL]) -> [String] {
        return jsonFiles.map({ (url) -> String in
            return url.absoluteString
        })
     }
}
