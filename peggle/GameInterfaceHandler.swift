//
//  GameInterfaceHandler.swift
//  peggle-physics-engine
//
//  Created by Sashankh Chengavalli Kumar on 15.02.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import Foundation

class GameInterfaceHandler {
    var ballLaunchAngle: Vector? = nil
    var ballLaunchedStatusInput: Bool = false
    var viewController: GamePlayViewController?

    init(viewController: GamePlayViewController?) {
        self.viewController = viewController
    }

    func resetGame() {
        ballLaunchedStatusInput = false
    }

    func getUserInput() {
        if viewController == nil {
            return
        }

        if let aimPoint = viewController!.latestAimPoint {
            let launchDirection = Vector(values: [
                Double(aimPoint.x), Double(aimPoint.y)
            ])

            setNewLaunchAngle(launchAngle: launchDirection)
        }
        
        setLaunchButton(launchNow: viewController!.ballLaunched)
    }

    func setNewLaunchAngle(launchAngle: Vector) {
        // View controller calls this
        ballLaunchAngle = launchAngle
    }

    func setLaunchButton(launchNow: Bool) {
        // View controller calls this
        if !ballLaunchedStatusInput {
            ballLaunchedStatusInput = launchNow
        }
    }

    func getCenterPoint() -> Vector {
        if viewController == nil {
            return Vector(dimension: 2)
        }
        return Vector(values: [
            Double(viewController!.boardWidth/2),
            Double(40)
        ])
    }

    func launchAngleIsValid(angleVector: Vector?) -> Bool {
        return (angleVector!.values[1] < 0)
    }

}
