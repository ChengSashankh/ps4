//
//  GameEngine.swift
//  peggle-physics-engine
//
//  Created by Sashankh Chengavalli Kumar on 15.02.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import Foundation
import UIKit

class GameLoop: NSObject {
    let gameInterfaceHandler: GameInterfaceHandler
    let gameState: GameState
    let gameRenderer: GameRenderer
    let frameRate: Int = 60
    let timeStep: Double
    var bounds = [Vector]()
    var viewControllerInstance: GamePlayViewController?
    var bucketPoint: CGPoint?

    init(gameRenderer: GameRenderer, gameState: GameState, gameInterfaceHandler: GameInterfaceHandler, viewController: GamePlayViewController?) {
        self.gameRenderer = gameRenderer
        self.gameState = gameState
        self.gameInterfaceHandler = gameInterfaceHandler
        self.timeStep = (1.0/Double(frameRate))
        self.viewControllerInstance = viewController
    }

    init(viewController: GamePlayViewController?) {
        gameRenderer = GameRenderer(viewController: viewController)
        gameInterfaceHandler = GameInterfaceHandler(viewController: viewController)
        timeStep = (1.0/Double(frameRate))
        viewControllerInstance = viewController


        if viewController != nil {
            let x = viewController!.boardx + viewController!.boardWidth/2
            let y = viewController!.boardy + viewController!.boardHeight - 40

            bucketPoint = CGPoint(x: x, y: y)

            bounds = [
                Vector(values: [0.0, 0.0]),
                Vector(values: [viewController!.boardWidth, 0.0]),
                Vector(values: [0.0, viewController!.boardHeight]),
                Vector(values: [viewController!.boardWidth, viewController!.boardHeight - 20])
            ]
        }

        gameState = GameState(centerPoint: viewController?.centerPoint, bucketPoint: bucketPoint, bounds: bounds)
    }

    func initializeGameStateWithLevel(level: Level) throws {
        try gameState.initializeStateWithLevel(level: level)
    }

    func resetGame() throws {
        try gameState.restartGame()
        gameInterfaceHandler.resetGame()
    }

    func gameLoop() {
        let displayLink = CADisplayLink(target: self, selector: #selector(gameLoopIteration))
        displayLink.preferredFramesPerSecond = frameRate
        displayLink.add(to: .current, forMode: .common)
    }

    @objc func gameLoopIteration(sender: CADisplayLink) throws {
        gameInterfaceHandler.getUserInput()

        try gameState.update(gameInterfaceHandler: gameInterfaceHandler, timeStep: timeStep)
        
        gameRenderer.renderCurrentGameState(gameState: gameState)
    }
}
