//
//  GamePlayViewController.swift
//  peggle-physics-engine
//
//  Created by Sashankh Chengavalli Kumar on 16.02.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import UIKit
import CoreGraphics

class GamePlayViewController: UIViewController {
    var ballLaunched: Bool = false
    var levelToShow: Level? = nil
    var latestAimPoint: CGPoint? = nil
    var lastTouchLocation: CGPoint? = nil
    var gameLoop: GameLoop = GameLoop(viewController: nil)
    var centerPoint: CGPoint = CGPoint()
    var ball: Ball? = nil
    var launchesLeft = 3
    var resultShown = false
    var cannonView: UIImageView? = nil

    var levelEditor = LevelEditor()
    var levelNames = [URL]()

    var componentToPeg = [UIImageView: Peg]()
    var pegToComponent = [Peg: UIImageView]()
    var pegsHighlighted: Set<Peg> = Set<Peg>()

    let ballWidth = 20
    let ballHeight = 20

    let pegWidth = 20
    let pegHeight = 20

    var targetMarker: UIImageView? = nil

    var boardWidth: Double = 0.0
    var boardHeight: Double = 0.0
    var boardx: Double = 0.0
    var boardy: Double = 0.0

    var uiPeggleBoard: UIImageView?

    // Outlets
    @IBOutlet weak var uiLevelNameLabel: UILabel!
//    @IBOutlet weak var uiPeggleBoard!: UIImageView!
    @IBOutlet weak var uiStartButton: UIButton!
    @IBOutlet weak var uiSecondsRemainingLabel: UILabel!
    @IBOutlet weak var uiPegsRemainingLabel: UILabel!
    @IBOutlet weak var uiScoreLabel: UILabel!
    @IBOutlet weak var uiMessageBoxLabel: UILabel!
    
    var uiBallComponent: UIImageView? = nil

    override func viewDidLoad() {
        let width = Double(self.view.frame.width) - 40.0
        let height = (600.0/800.0) * width

        assert (width > 0)

        uiPeggleBoard = UIImageView(
            frame: CGRect(
                origin: CGPoint(x: 20, y: 250),
                size: CGSize(width: width, height: height)
            )
        )

        uiPeggleBoard!.image = UIImage(named: "background-anatomy.png")!

        self.view.addSubview(uiPeggleBoard!)
        self.view.bringSubviewToFront(uiPeggleBoard!)

        boardWidth = width
        boardHeight = height
        boardx = 20
        boardy = 250
        centerPoint = CGPoint(x: boardWidth/2, y: 40)

        gameLoop = GameLoop(viewController: self)
        loadLevelToShow()

        initializeBall()
        gameLoop.gameState.ball = ball!
        renderBall(ball: ball!)

        // Add gesture recognizer for board
        addTapGestureRecognizerForBoard()

        // Share instance with game loop
        gameLoop.viewControllerInstance = self
        executeGameLoop()
        super.viewDidLoad()

        // Start music play
        // TODO: Fix music play
//        BackgroundMusicHelper.backgroundMusicHelperInstance.playBackgroundMusic(fileName: "background")

        setScore(score: gameLoop.gameState.currentScore)
        setSecondsRemaining(seconds: gameLoop.gameState.timeLeft)
        setPegsRemaining(pegsRemaining: gameLoop.gameState.pegsRemaining)
        setUIMessageBoxLabel(message: "Time to launch!")
    }

    override func viewDidAppear(_ animated: Bool) {
        drawCannon()
        showRulesAlert()
    }

    // To show toast messages (error/info) for specified number of seconds
    func showToastMessage(message: String, seconds: Double, mode: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.alpha = 0.5
        alert.view.layer.cornerRadius = 15

        if mode == "error"{
            alert.view.backgroundColor = .red
        } else {
            alert.view.backgroundColor = .white
        }

        let okAction = UIAlertAction(title: "Got it!", style: .cancel) {
            UIAlertAction in
        }

        alert.addAction(okAction)

        self.present(alert, animated: true)

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            alert.dismiss(animated: true)
        }
    }

    func showRulesAlert() {
        showToastMessage(
            message: "Welcome to the Anatonmy Park episode from Rick and Morty! Much like in the show, it is time to shoot virus cells to save a life! Tap on a point to aim, and then click start. The ball will relaunch a number of times randomly. You must destroy all cells to win.",
            seconds: 10.0,
            mode: "info"
        )
    }

    func drawCannon() {
        cannonView = UIImageView(image: UIImage(named: "cannon.png"))
        cannonView!.frame = CGRect(
            x: boardWidth/2 - 30,
            y: 2,
            width: 60,
            height: 45
        )

        self.uiPeggleBoard!.addSubview(cannonView!)
        self.uiPeggleBoard!.bringSubviewToFront(cannonView!)
    }

    func initializeBall() {
        // Initialize ball
        do {
            try gameLoop.gameState.createBall(centerPoint: centerPoint)
            ball = gameLoop.gameState.ball
        } catch {
            ball = Ball(
                ballLocation: LevelEntityLocation(xLocation: Double(centerPoint.x), yLocation: Double(centerPoint.y))
            )
        }
    }

    func setScore(score: Int) {
        uiScoreLabel.text = String("\(score) points")
    }

    func setSecondsRemaining(seconds: Double) {
        if seconds > Double(Int(seconds)) {
            uiSecondsRemainingLabel.text = String("Time: \(Int(seconds) + 1)")
        } else {
            uiSecondsRemainingLabel.text = String("Time: \(Int(seconds))")
        }
    }

    func setPegsRemaining(pegsRemaining: Int) {
        uiPegsRemainingLabel.text = String("Pegs Remaining: \(pegsRemaining)")
    }

    func setUIMessageBoxLabel(message: String) {
        uiMessageBoxLabel.text = message
    }

    func loadLevel(fileName: String) {
        // Clear the existing level from screen
        for pegImageView in componentToPeg.keys {
            _ = levelEditor.removePeg(pegToRemove: componentToPeg[pegImageView]!)
            pegImageView.removeFromSuperview()
        }

        uiLevelNameLabel.text = nil

        // Load and show new level
        levelEditor.loadFromFile(fileName: fileName)
        renderLevelUI(levelToShow: levelEditor.getLevel())

        // TODO: Create the bucket
    }

    func loadLevelToShow() {
        levelToShow = SampleGameLoader().getSampleLevel()
        renderLevelUI(levelToShow: levelToShow)
        do {
            try gameLoop.initializeGameStateWithLevel(level: levelToShow!)
        } catch {
            print ("Could not initialize the level in game state")
        }
    }

    func renderLevelUI(levelToShow: Level?) {
        // Clear the data structures
        self.componentToPeg = [UIImageView: Peg]()
        self.pegToComponent = [Peg: UIImageView]()

        // Create the UI views
        uiLevelNameLabel.text = levelToShow?.name

        for (index, peg) in levelToShow!.pegList.enumerated() {
            peg.id = index

            let pegUIView = renderPegUiComponent(
                touchLocation: CGPoint(x: peg.pegLocation.xLocation, y: peg.pegLocation.yLocation),
                pegColor: peg.pegColor,
                pegShape: peg.pegShape
            )
            self.componentToPeg[pegUIView] = peg
            self.pegToComponent[peg] = pegUIView

            UIView.transition(with: self.view, duration: 0.30, options: [.transitionCrossDissolve], animations: {
              self.uiPeggleBoard!.addSubview(pegUIView)
            }, completion: nil)

            self.uiPeggleBoard!.bringSubviewToFront(pegUIView)

            // TODO: remove debugging code
//            let marker = UILabel()
//            marker.frame = CGRect(
//                x: Double(peg.pegLocation.xLocation),
//                y: Double(peg.pegLocation.yLocation),
//                width: Double(20),
//                height: Double(20)
//            )
//
//            marker.backgroundColor = .black
//            self.uiPeggleBoard!.addSubview(marker)
//            self.uiPeggleBoard!.bringSubviewToFront(marker)
        }
    }

//    func getBallStartingPoint() -> CGPoint {
//        var x: Double = 0.0
//        var y: Double = 0.0
//
//        let width = self.view.frame.width
//
//        x = Double(width / 2) - 20
//        y = 40
//
//        return CGPoint(x: x, y: y)
//    }

    func addTapGestureRecognizerForBoard() {
        let tapGesture = UITapGestureRecognizer(target: self, action: Selector(("onPeggleBoardTap")))
        uiPeggleBoard!.addGestureRecognizer(tapGesture)
        uiPeggleBoard!.isUserInteractionEnabled = true
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is LoadFileViewController {
            let destinationController = segue.destination as? LoadFileViewController
            destinationController?.levelNames = levelEditor.listLevelsToLoad()
            destinationController?.gamePlayViewController = self
        }
    }

    @IBAction func onLoadButtonClick(_ sender: Any) {
        levelNames = levelEditor.listLevelsToLoad()
    }

    /********************** Creating UI objects ******************************/

    func renderBall(ball: Ball) {
        uiBallComponent = UIImageView(
            frame: CGRect(
                x: Double(ball.ballLocation.xLocation),
                y: Double(ball.ballLocation.yLocation),
                width: Double(ballWidth),
                height: Double(ballHeight)
            )
        )

        let imageName = "ball.png"
        let image = UIImage(named: imageName)
        uiBallComponent!.image = image!

        self.uiPeggleBoard!.addSubview(uiBallComponent!)
        self.uiPeggleBoard!.bringSubviewToFront(uiBallComponent!)
    }

    func renderPegUiComponent(touchLocation: CGPoint, pegColor: String, pegShape: String) -> UIImageView {
        let pegImageView = UIImageView(
            frame: CGRect(
                x: Double(touchLocation.x) - Double(pegWidth)/2.0,
                y: Double(touchLocation.y) - Double(pegHeight)/2.0,
                width: Double(pegWidth),
                height: Double(pegHeight)
            )
        )

        switch [pegColor, pegShape] {
            case ["Blue", "Circle"]: pegImageView.image = UIImage(named: "peg-blue.png")!
            case ["Blue", "Triangle"]: pegImageView.image = UIImage(named: "peg-blue-triangle.png")!
            case ["Red", "Circle"]: pegImageView.image = UIImage(named: "peg-red.png")!
            case ["Red", "Triangle"]: pegImageView.image = UIImage(named: "peg-red-triangle.png")!
            case ["Green", "Circle"]: pegImageView.image = UIImage(named: "peg-green.png")!
            case ["Green", "Triangle"]: pegImageView.image = UIImage(named: "peg-green-triangle.png")!
            default: print ("Peg type not recognised")
        }

        self.view.addSubview(pegImageView)
        return pegImageView
    }

    func renderTargetMarker(location: CGPoint) {
        if targetMarker != nil {
            targetMarker?.removeFromSuperview()
        } else {
            targetMarker = UIImageView(image: UIImage(
                named: "crosshairs.jpg"
            ))
        }

        if latestAimPoint == nil {
            return
        }
        targetMarker!.frame = CGRect(
            x: Double(latestAimPoint!.x - 5),
            y: Double(latestAimPoint!.y - 5),
            width: Double(15),
            height: Double(15)
        )

        self.view.addSubview(targetMarker!)
        self.view.bringSubviewToFront(targetMarker!)

        let angleOfRotation = getAngleFromAim(aimPoint: latestAimPoint!, centerPoint: centerPoint)

        cannonView!.transform = CGAffineTransform(rotationAngle: CGFloat(angleOfRotation))
    }

    func getAngleFromAim(aimPoint: CGPoint, centerPoint: CGPoint) -> Double {
        let aimPointVector = Vector(values: [
            Double(aimPoint.x),
            Double(aimPoint.y)
            ]
        )

        let centerPointVector = Vector(values: [
            Double(centerPoint.x),
            Double(centerPoint.y)
            ]
        )

        aimPointVector.subtract(otherVector: centerPointVector)
        let downwardDirection = Vector(values: [0.0, 1.0])
        var angle = acos(downwardDirection.cosOfAngleWith(otherVector: aimPointVector))

        if aimPoint.x > centerPoint.x {
            angle = -angle
        }
        return angle
    }

    func highlightPegs(pegsToHighlight: [Peg]) {
        for peg in pegsToHighlight {
            let uiComponentToHighlight = pegToComponent[peg]

            switch [peg.pegColor, peg.pegShape] {
                case ["Blue", "Circle"]: uiComponentToHighlight!.image = UIImage(named: "peg-blue-glow.png")!
                case ["Blue", "Triangle"]: uiComponentToHighlight!.image = UIImage(named: "peg-blue-glow-triangle.png")!
                case ["Red", "Circle"]: uiComponentToHighlight!.image = UIImage(named: "peg-red-glow.png")!
                case ["Red", "Triangle"]: uiComponentToHighlight!.image = UIImage(named: "peg-red-glow-triangle.png")!
                case ["Green", "Circle"]: uiComponentToHighlight!.image = UIImage(named: "peg-green-glow.png")!
                case ["Green", "Triangle"]: uiComponentToHighlight!.image = UIImage(named: "peg-green-glow-triangle.png")!
                default: print ("Peg type not recognised")
            }
        }
    }

    func removePegs(pegsToRemove: [Peg]) {
        for peg in pegsToRemove {
            UIView.transition(
                with: self.view, duration: 0.45, options: [.transitionCrossDissolve], animations: {
                    self.pegToComponent[peg]?.removeFromSuperview()
            }, completion: nil)
        }
    }

    func showGameState(gameState: GameState) {
        if resultShown {
            return
        }

        if gameState.spaceBlastInProgress {
            self.view.backgroundColor = .green
            self.uiBallComponent!.layer.shadowColor = UIColor.green.cgColor
            self.uiBallComponent!.layer.shadowRadius = 60
            self.uiBallComponent!.layer.shadowOpacity = 0.5
            self.uiBallComponent!.layer.shadowPath = UIBezierPath(rect: uiBallComponent!.bounds).cgPath
            self.uiBallComponent!.layer.shadowOffset = .zero
        } else {
            self.view.backgroundColor = .white
        }

        // Update displays
        setScore(score: gameState.currentScore)
        setSecondsRemaining(seconds: gameState.timeLeft)
        setPegsRemaining(pegsRemaining: gameState.pegsRemaining)
        setUIMessageBoxLabel(message: gameState.currentMessage)

        // Remove the current ball
        uiBallComponent!.removeFromSuperview()

        // Highlight the pegs that are hit
        highlightPegs(pegsToHighlight: gameState.objectsToHighlight.map({ (particle) -> Peg in
            return particle.payload as! Peg
        }))

        gameState.objectsToHighlight.removeAll()

        // Render the new ball
        renderBall(ball: gameState.ball)

        if gameState.gameEnded {
            let pegsToRemove = gameState.objectsToRemove.map { (particle) -> Peg in
                return particle.payload as! Peg
            }
            gameState.objectsToRemove.removeAll()

            uiBallComponent?.removeFromSuperview()

            if launchesLeft == 0 {
                // Check win or lose
                if gameState.pegsRemaining > 0 {
                    let message = "YOU LOSE! \(gameState.pegsRemaining) virus cells survived. To start a new game please go back to home screen, and return here to play!"

                    showToastMessage(message: message, seconds: 5.0, mode: "error")
                } else {
                    let message = "YOU WIN! You killed the virus. To start a new game please go back to home screen, and return here to play!"

                    showToastMessage(message: message, seconds: 5.0, mode: "alert")
                }
                resultShown = true

                return
            }

            do {
                try gameLoop.gameState.restartGame()
            } catch {
            }

            launchesLeft -= 1

            removePegs(pegsToRemove: pegsToRemove)

            initializeBall()
            gameLoop.gameState.ball = ball!
            let ballParticle = gameLoop.gameState.ballParticle
            gameLoop.gameState.physicsEngine.removeParticleFromWorld(particle: ballParticle)
            ballLaunched = false
            renderBall(ball: gameState.ball)
        }
    }

    /******************** Carry out game actions  ***************************/

    func setNewAimPoint(tapLocation: CGPoint) {
        latestAimPoint = tapLocation
        renderTargetMarker(location: tapLocation)
    }

    func launchBall() {
        ballLaunched = true
        if targetMarker != nil {
            targetMarker?.removeFromSuperview()
        }
    }

    func executeGameLoop() {
        gameLoop.gameLoop()
    }

    /******************** Action Triggers from UI ***************************/

    @objc func onPeggleBoardTap() {
        if !ballLaunched {
            setNewAimPoint(tapLocation: lastTouchLocation!)
        }
     }

    @IBAction func onStartButtonPress(_ sender: Any) {
        if !ballLaunched && lastTouchLocation != nil {
            launchBall()
        }
    }

    // Get and store latest touch location
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = event?.allTouches?.first {
            let loc: CGPoint = touch.location(in: touch.window)
            lastTouchLocation = loc
        }
    }

}
