//
//  GameRenderer.swift
//  peggle-physics-engine
//
//  Created by Sashankh Chengavalli Kumar on 14.02.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import UIKit

class GameRenderer {
    var viewController: GamePlayViewController?

    init(viewController: GamePlayViewController?) {
        self.viewController = viewController
    }

    func renderCurrentGameState(gameState: GameState) {
        if viewController != nil {
            viewController!.showGameState(gameState: gameState)
        }
    }
}
