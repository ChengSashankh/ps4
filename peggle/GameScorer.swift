//
//  GameScorer.swift
//  peggle
//
//  Created by Sashankh Chengavalli Kumar on 01.03.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import Foundation

class GameScorer {
    var score = 0

    let launchBallBonus = 2

    let orangePegPoints = 100
    let bluePegPoints = 10
    let greenPegPoints = 10

    let spaceBlastBonus = 20
    let spookyBallBonus = 30
    let bucketBonus = 50

    func launchBall() {
        score += launchBallBonus
    }

    func hitOrangePeg() {
        score += orangePegPoints
    }

    func hitBluePeg() {
        score += bluePegPoints
    }

    func hitGreenPeg() {
        score += greenPegPoints
    }

    func hitSpaceBlast() {
        score += spaceBlastBonus
    }

    func hitSpookyBall() {
        score += spookyBallBonus
    }

    func hitBucket() {
        score += bucketBonus
    }

    func getScore() -> Int {
        return score
    }

    func resetScore() {
        score = 0
    }

    func addScore(points: Int) {
        score += points
    }

    func multiplyScore(factor: Int) {
        score *= factor
    }
}
