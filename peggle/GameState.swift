//
//  GameState.swift
//  peggle-physics-engine
//
//  Created by Sashankh Chengavalli Kumar on 14.02.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import Foundation
import CoreGraphics

enum PowerUpStatus {
    case spaceBlast
    case spookyBall
    case notInProgress
}

class GameState {
    var ball: Ball
    var ballParticle: Particle<PhysicsEngineTrackableObject>
    var bucket: Bucket
    var level: Level = Level()
    var ballLaunched: Bool = false
    var ballLaunchDirection: Vector = Vector(dimension: 2)
    var gameEnded: Bool = false
    var objectsToRemove = [Particle<PhysicsEngineTrackableObject>]()
    var objectsToHighlight = [Particle<PhysicsEngineTrackableObject>]()
    var spookyBallInProgress = false
    var spaceBlastInProgress = false

    var physicsEngine : PhysicsEngine = PhysicsEngine()
    var objectToParticle: [PhysicsEngineTrackableObject: Particle<PhysicsEngineTrackableObject>] = [PhysicsEngineTrackableObject: Particle]()

    var gameScorer: GameScorer = GameScorer()
    var currentScore: Int = 0
    var currentMessage: String = "Let's get started!"

    // TODO: Tune this value
    let spaceBlastRadius: Double = 100.0
    let bucketZoneName: String = "Bucket"

    var timeLeft = 8.0

    // 60 seconds at 60 FPS
    var framesLeft: Int = 8 * 60
    var pegsRemaining: Int = 0
    var centerPoint: CGPoint?

    init(centerPoint: Vector?, bucketPoint: Vector?, bounds: [Vector]) {
        if centerPoint == nil {
            ball = Ball(ballLocation:LevelEntityLocation(vector: Vector(dimension: 2)))
            ballParticle = Particle<PhysicsEngineTrackableObject>(
                dim: 2,
                mass: 10,
                initialPosition: Vector(dimension: 2),
                initialVelocity: Vector(dimension: 2),
                initialAcceleration: Vector(dimension: 2),
                movable: true,
                payload: ball
            )

            print ("center point is nil")
        } else {
            ball = Ball(ballLocation:LevelEntityLocation(vector: centerPoint!))
            ballParticle = Particle<PhysicsEngineTrackableObject>(
                dim: 2,
                mass: 10,
                initialPosition: Vector(dimension: 2),
                initialVelocity: Vector(dimension: 2),
                initialAcceleration: Vector(dimension: 2),
                movable: true,
                payload: ball
            )

        }

        if bucketPoint == nil {
            bucket = Bucket(position: LevelEntityLocation(vector: Vector(dimension: 2)), vertices: [])
            print ("center point is nil")
        } else {
            // TODO: Get the bucket position here
            bucket = Bucket(position: LevelEntityLocation(vector: Vector(dimension: 2)), vertices: [])
        }

        // TODO: Get this from constructor (supplied dynamically by the view controller in-charge)
        physicsEngine.setBounds(bounds: bounds)

        self.centerPoint = CGPoint(x: centerPoint?.values[0] ?? 0, y: centerPoint?.values[1] ?? 0)
    }

    init(centerPoint: CGPoint?, bucketPoint: CGPoint?, bounds: [Vector]) {
        if centerPoint == nil {
            print ("center point is nil")
            ball = Ball(ballLocation:LevelEntityLocation(vector: Vector(dimension: 2)))
            ballParticle = Particle<PhysicsEngineTrackableObject>(
                dim: 2,
                mass: 10,
                initialPosition: Vector(dimension: 2),
                initialVelocity: Vector(dimension: 2),
                initialAcceleration: Vector(dimension: 2),
                movable: true,
                payload: ball
            )
        } else {
            let centerPointVector = Vector(values: [
                Double(centerPoint!.x),
                Double(centerPoint!.y)
            ])
            ball = Ball(ballLocation: LevelEntityLocation(vector: centerPointVector))
            ballParticle = Particle<PhysicsEngineTrackableObject>(
                dim: 2,
                mass: 10,
                initialPosition: Vector(dimension: 2),
                initialVelocity: Vector(dimension: 2),
                initialAcceleration: Vector(dimension: 2),
                movable: true,
                payload: ball
            )
        }

        if bucketPoint == nil {
            bucket = Bucket(position: LevelEntityLocation(vector: Vector(dimension: 2)), vertices: [])
            print ("center point is nil")
        } else {
            // TODO: Get the bucket position here
            bucket = Bucket(
                position: LevelEntityLocation(
                    xLocation: Double(bucketPoint!.x),
                    yLocation: Double(bucketPoint!.y)
                ),
                vertices: []
            )
        }

        physicsEngine.setBounds(bounds: bounds)

        self.centerPoint = centerPoint
    }

    func initializeStateWithLevel(level: Level) throws {
        self.level = level

        pegsRemaining = level.pegList.count

        // Create the particles in the physics engine
        for peg in level.pegList {
            let particle = Particle<PhysicsEngineTrackableObject>(
                dim: 2,
                mass: 100000.0,
                initialPosition: Vector(values: [peg.pegLocation.xLocation, peg.pegLocation.yLocation]),
                initialVelocity: Vector(dimension: 2),
                initialAcceleration: Vector(dimension: 2),
                movable: false,
                payload: peg
            )

            if peg.pegShape == "Triangle" {
                particle.boundingMethod = BoundingType.TRIANGULAR
                particle.boundingPoints = physicsEngine.getTriangleVerticlesFromPos(
                    pos: CGPoint(x: peg.pegLocation.xLocation, y: peg.pegLocation.yLocation)
                )
            }

            objectToParticle[peg] = particle

            physicsEngine.addParticleToWorld(particle: particle)
        }
    }

    func createBall(centerPoint: CGPoint) throws {
        // Create the ball object
        ball = Ball(ballLocation: LevelEntityLocation(vector: Vector(values: [Double(centerPoint.x), Double(centerPoint.y)])))

        // Create the ball to launch
        let ballParticle = Particle<PhysicsEngineTrackableObject>(
            dim: 2,
            mass: 10,
            initialPosition: Vector(values: [ball.ballLocation.xLocation, ball.ballLocation.yLocation]),
            initialVelocity: Vector(dimension: 2),
            initialAcceleration: Vector(dimension: 2),
            movable: true,
            payload: ball
        )

        ballParticle.boundingRadius = 20

        // Add ball to physics engine world
        physicsEngine.addParticleToWorld(particle: ballParticle)
    }

    fileprivate func updateTimeRemaining() {
        if ballLaunched && !gameEnded {
            framesLeft -= 1
            timeLeft = Double(framesLeft) / 60.0
        }
    }

    func restartGame() throws {
        ballLaunched = false
        gameEnded = false

        timeLeft = 8.0
        framesLeft = 8 * 60
        pegsRemaining = level.pegList.count

        physicsEngine.removeParticleFromWorld(particle: ballParticle)

        try createBall(centerPoint: centerPoint!)
    }

    /********************** Update functions ******************************/

    func update(gameInterfaceHandler: GameInterfaceHandler, timeStep: Double) throws {
        checkIfGameEnded()
        if gameEnded {
            handleGameEndedEvent()
            return
        }

        updateTimeRemaining()

        // Update based on user input
        try updateBasedOnInput(
            gameInterfaceHandler: gameInterfaceHandler,
            centerPoint: gameInterfaceHandler.getCenterPoint()
        )

        // Update particles in motion and update game state
        try updateBasedOnMotion(timeStep)

        currentScore = gameScorer.getScore()

        // Set a message based on the current score
        setMessageBasedOnScore(score: currentScore)
    }

    // Given the user input, update everything you need to do in the game state
    func updateBasedOnInput(gameInterfaceHandler: GameInterfaceHandler, centerPoint: Vector) throws {
        // Update the direction in the game state
        if gameInterfaceHandler.ballLaunchAngle != nil {
            ballLaunchDirection = gameInterfaceHandler.ballLaunchAngle!
        }

        // Update the ball launched status based on user input
        if gameInterfaceHandler.ballLaunchedStatusInput && !ballLaunched {
            ballLaunched = true
            try launchBallAtAngle(clickPoint: ballLaunchDirection, centerPoint: centerPoint)
        }
    }

    func updateBasedOnMotion(_ timeStep: Double) throws {
        // Handle the collisions
        let collisions = try getCollisions()
        try handleCollisions(collisions: collisions)

        // First update all particles
        // - Update the particles in the physics engine
        // - Update the game state from the physics engine
        let changes = try physicsEngine.updateAfter(seconds: timeStep)
        for changedParticle in changes {
            try changedParticle.payload!.setLocation(vector: changedParticle.position)
            if changedParticle.payload! is Ball {
                ball = changedParticle.payload as! Ball
            }
        }
    }

    // TODO: Fine tune message and scores
    func setMessageBasedOnScore(score: Int) {
        if score < 1000 {
            currentMessage = "Ooh! Now we're going!"
        } else if score < 2000 {
            currentMessage = "Nice run!"
        } else if score < 4000 {
            currentMessage = "Ooh well played!"
        } else if score < 6000 {
            currentMessage = "Getting to 6000!!"
        } else {
            currentMessage = "Insane run!"
        }
    }

    /********************** Handle Collisions  ******************************/

    func getCollisions() throws -> Set<Set<Particle<PhysicsEngineTrackableObject>>> {
        return try physicsEngine.getCollisions()
    }

    func handleCollisions(collisions: Set<Set<Particle<PhysicsEngineTrackableObject>>>) throws {
        // Handle physics object collisions
        let collisions = physicsEngine.handleCollisions(collisions: collisions)

        // Handle the impact of collision on game objects
        for collision in collisions {
            for object in collision {
                if object.payload is Peg {
                    markPegAsHit(particle: object)
                    let powerUpTriggered = wasPowerUpTriggered(particle: object)
                    if powerUpTriggered {
                        triggerRandomPowerUp()
                        try performPowerUpOperation(particle: object)
                    }
                } else {
                    // TODO
//                    try updatePositionOfBall(ball: object.payload as! Ball, newPosition: object.position)
                }
            }
        }
    }

    func markPegAsHit(particle: Particle<PhysicsEngineTrackableObject>) {
        if !(particle.payload is Peg) {
            return
        }

        let peg = particle.payload as! Peg

        switch peg.pegColor {
            case "Green": gameScorer.hitGreenPeg()
            case "Blue": gameScorer.hitBluePeg()
            case "Orange": gameScorer.hitOrangePeg()
            default: break
        }

        queueParticleForRemoval(particle: particle)
        highlightParticle(particle: particle)
    }

    func queueParticleForRemoval(particle: Particle<PhysicsEngineTrackableObject>) {
        objectsToRemove.append(particle)
    }

    func highlightParticle(particle: Particle<PhysicsEngineTrackableObject>) {
        objectsToHighlight.append(particle)
    }


    /********************** Game actions implementation ******************************/

    func checkIfGameEnded() {
        if framesLeft == 0 {
            gameEnded = true
            // TODO: Show timer elapsed animations/effects/messages here
        }
        // TODO: find way to check for ball exited bottom condition
    }

    fileprivate func handleGameEndedEvent() {
        // Actions to perform after game has ended
        for object in objectsToRemove {
            removePeg(peg: object.payload as! Peg)
        }

        pegsRemaining = level.pegList.count
        ballParticle.velocity = Vector(dimension: 2)
        ballParticle.acceleration = Vector(dimension: 2)
    }

    // Implement powerups

    func performPowerUpOperation(particle: Particle<PhysicsEngineTrackableObject>) throws {
        if spaceBlastInProgress {
            try performSpaceBlastUpdate(particle: particle)
        }

        if spookyBallInProgress {
             performLifeLinePowerUp(particle: particle)
        }
    }

    func performSpaceBlastUpdate(particle: Particle<PhysicsEngineTrackableObject>) throws {
        let pegs = try getPegsWithinSpaceBlastRadius(particle: particle)
        for peg in pegs {
            markPegAsHit(particle: peg)
        }
    }

    func getPegsWithinSpaceBlastRadius(particle: Particle<PhysicsEngineTrackableObject>) throws -> [Particle<PhysicsEngineTrackableObject>] {
        var pegsWithinRadius = [Particle<PhysicsEngineTrackableObject>]()

        for candidateParticle in physicsEngine.particleWatcher.particlesInScene {
            let distance = try candidateParticle.position.distanceTo(otherVector: particle.position)

            if distance > 0.0 && distance <= spaceBlastRadius {
                pegsWithinRadius.append(candidateParticle)
            }
        }

        return pegsWithinRadius
    }

    func performLifeLinePowerUp(particle: Particle<PhysicsEngineTrackableObject>) {
        let xBall = ball.ballLocation.xLocation

        let possibleBalls = physicsEngine.particleWatcher.particlesInScene.filter { $0.payload is Ball }

        for ballParticle in possibleBalls {
            let ballPos = ballParticle.position

            let lowerBound = physicsEngine.particleWatcher.bounds[3].values[1]

            if ballPos.values[1] > lowerBound - 300 {
                ballParticle.position = Vector(values: [ballPos.values[0], 21])
            }
        }

        // Set the new location of the ball as 0
        // TODO: Is zero the correct value
        ball.ballLocation = LevelEntityLocation(xLocation: xBall, yLocation: 40)
    }

    func wasPowerUpTriggered(particle: Particle<PhysicsEngineTrackableObject>) -> Bool {
        let payload = particle.payload

        if payload is Peg {
            let peg = payload as! Peg

            if peg.pegColor == "Green" {
                return true
            }
        }

        return false
    }

    // TODO: Test this function
    func triggerRandomPowerUp() {
        let randomChoiceIndex = Int.random(in: 0..<2)

        if randomChoiceIndex == 0 {
            startSpaceBlast()
            gameScorer.hitSpaceBlast()
        } else {
            startSpookyBall()
            gameScorer.hitSpookyBall()
        }
    }

    func startSpaceBlast() {
        spaceBlastInProgress = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
            self.spaceBlastInProgress = false
        })
    }

    func startSpookyBall() {
        spookyBallInProgress = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 7.0, execute: {
            self.spaceBlastInProgress = false
        })
    }

    // Implement bucket operations

    func isBallInBucket() -> Bool {
        let particlesInZonesOfInterest = physicsEngine.getObjectsInZonesOfInterest()

        for (particle, zones) in particlesInZonesOfInterest {
            if particle.payload is Ball {
                for zone in zones {
                    if zone.zoneName == bucketZoneName {
                        return true
                    }
                }
            }
        }

        return false
    }

    func checkAndHandleBallInBucket() {
        if isBallInBucket() {
            gameScorer.hitBucket()
            handleBallInBucket()
        }
    }

    // TODO: Implement
    func handleBallInBucket() {
        // Perform some action when ball ball is in bucket
        // TODO: Tint screen?
        // TODO: Play audio?
        // TODO: Show message?
    }

    func launchBallAtAngle(clickPoint: Vector, centerPoint: Vector) throws {
        let initialVelocity = try getInitialVelocityFromDirection(
            clickPoint: clickPoint, centerPoint: centerPoint, withGravity: true)

        // Create the ball object
        ball.ballLocation = LevelEntityLocation(vector: centerPoint)

        // Create the ball to launch
        ballParticle = try Particle<PhysicsEngineTrackableObject>(
            dim: 2,
            mass: 10,
            initialPosition: centerPoint,
            initialVelocity: initialVelocity,
            initialAcceleration: getAccelerationDueToGravity(),
            movable: true,
            payload: ball
        )

        ballParticle.boundingRadius = 20

        // Add ball to physics engine world
        physicsEngine.addParticleToWorld(particle: ballParticle)
    }

    func removePeg(peg: Peg) {
        _ = level.removePeg(pegToRemove: peg)

        // Remove from physics engine
        physicsEngine.removeParticleFromWorld(particle: objectToParticle[peg]!)
    }

    func updatePositionOfBall(ball: Ball, newPosition: Vector) throws {
        try ball.setLocation(vector: newPosition)
    }

    /********************** Support methods for velocity and acceleration ******************************/

    func getInitialVelocityFromDirection(clickPoint: Vector, centerPoint: Vector, withGravity: Bool) throws -> Vector {
        let initialVelocity = Vector(dimension: 2)
        try initialVelocity.add(otherVector: clickPoint)
        try initialVelocity.subtract(otherVector: centerPoint)

        // Normalize vector
        initialVelocity.scale(constant: 700 / initialVelocity.norm())

        return initialVelocity
    }

    func getAccelerationDueToGravity() -> Vector {
        return Vector(values: [0.0, 980.0])
    }
}
