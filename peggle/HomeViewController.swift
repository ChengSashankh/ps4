//
//  HomeViewController.swift
//  peggles
//
//  Created by Sashankh Chengavalli Kumar on 28.01.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var playButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "homepage.png")
        backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
        self.view.insertSubview(backgroundImage, at: 0)

        createButton.backgroundColor = .black
        createButton.layer.cornerRadius = 15
        exitButton.backgroundColor = .black
        exitButton.layer.cornerRadius = 15
        playButton.backgroundColor = .black
        playButton.layer.cornerRadius = 15
    }

    @IBAction func onCreateLevelButtonClick(_ sender: UIButton) {
        // Directs to create level view controller.
        // Empty function left here to "document" segue in code
    }

    @IBAction func onExitButtonClick(_ sender: UIButton) {
        exit(1)
    }
}
