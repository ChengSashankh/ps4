//
//  GameScreen.swift
//  peggles
//
//  Created by Sashankh Chengavalli Kumar on 28.01.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import Foundation

class Level: Codable & Equatable {
    var pegList: [Peg] = [Peg]()
    var pegLocations: Set<LevelEntityLocation> = Set<LevelEntityLocation>()
    let validColors: [String] = ["Red", "Blue", "Green"]
    var lastUpdated: Date = Date()
    var name: String?

    let minimumPegDistance: Double = 20

    private enum CodingKeys: String, CodingKey {
        case pegList
        case pegLocations
        case validColors
        case lastUpdated
        case name
    }

    init() {
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pegList = try values.decode([Peg].self, forKey: .pegList)
        pegLocations = try values.decode(Set<LevelEntityLocation>.self, forKey: .pegLocations)
        lastUpdated = try values.decode(Date.self, forKey: .lastUpdated)
        name = try values.decode(String.self, forKey: .name)
    }

    static func == (lhs: Level, rhs: Level) -> Bool {
        return
            lhs.pegList == rhs.pegList &&
            lhs.pegLocations == rhs.pegLocations &&
            lhs.validColors == rhs.validColors &&
            lhs.lastUpdated == rhs.lastUpdated
    }

    func setLastUpdatedToNow() {
        lastUpdated = Date()
    }

    func setName(name: String) {
        self.name = name
    }

    func pegColorIsValid(pegColor: String) -> Bool {
        return validColors.contains(pegColor)
    }

    // Eucledian distance between points
    func getPegDistance(fromPegLocation: LevelEntityLocation, toPegLocation: LevelEntityLocation) -> Double {
        let squaredDistance =
            pow(fromPegLocation.xLocation - toPegLocation.xLocation, 2) +
            pow(fromPegLocation.yLocation - toPegLocation.yLocation, 2)

        return pow(squaredDistance, 0.5)
    }

    func checkMinimumDistanceConstraint(newPegLocation: LevelEntityLocation) -> Bool {
        for pegLocation in pegLocations {
            if getPegDistance(fromPegLocation: pegLocation, toPegLocation: newPegLocation) < minimumPegDistance {
                return false
            }
        }

        return true
    }

    func pegCanBeAdded(peg: Peg) -> Bool {
        return
            pegColorIsValid(pegColor: peg.pegColor) &&
            checkMinimumDistanceConstraint(newPegLocation: peg.pegLocation)
    }

    func pegExists(peg: Peg) -> Bool {
        return pegList.contains(peg)
    }

    // If peg cannot be added due to (location clash/invalid color), return nil
    func addPeg(pegToAdd: Peg) -> Peg? {
        if !pegCanBeAdded(peg: pegToAdd) {
            return nil
        }

        pegList.append(pegToAdd)
        pegLocations.insert(pegToAdd.pegLocation)
        setLastUpdatedToNow()

        return pegToAdd
    }

    // If peg doesn't exist, do nothing
    func removePeg(pegToRemove: Peg) -> Peg? {
        if !pegExists(peg: pegToRemove) {
            return nil
        }

        pegLocations.remove(pegToRemove.pegLocation)
        if let index = pegList.firstIndex(of: pegToRemove) {
            pegList.remove(at: index)
        }
        setLastUpdatedToNow()

        return pegToRemove
    }

    func removeAllPegs() {
        pegList.removeAll()
        pegLocations.removeAll()
        setLastUpdatedToNow()
    }

    func encodeAsJson() -> String {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted

        let data = (try? encoder.encode(self)) ?? Data()
        return String(data: data, encoding: .utf8)!
    }
}
