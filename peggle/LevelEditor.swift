//
//  LevelEditor.swift
//  peggles
//
//  Created by Sashankh Chengavalli Kumar on 29.01.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import Foundation

class LevelEditor {
    private var level: Level

    init(decoder: Decoder) {
        level = (try? Level(from: decoder)) ?? Level()
    }

    init() {
        level = Level()
    }

    init(level: Level) {
        self.level = level
    }

    func changeLevel(newLevel: Level) {
        level = newLevel
    }

    func setLevelName(newName: String) {
        level.setName(name: newName)
    }

    func getLevelName() -> String? {
        return level.name
    }

    func getLevel() -> Level {
        return level
    }

    func mapLocationToPeg(xCoordinate: Double, yCoordinate: Double) -> Peg? {
        let clickLocation = LevelEntityLocation(xLocation: xCoordinate, yLocation: yCoordinate)

        for peg in level.pegList {
            if level.getPegDistance(fromPegLocation: clickLocation, toPegLocation: peg.pegLocation) <
                level.minimumPegDistance {
                return peg
            }
        }

        return nil
    }

    func addPeg(pegToAdd: Peg) -> Bool {
        return level.addPeg(pegToAdd: pegToAdd) != nil
    }

    func removePeg(pegToRemove: Peg) -> Bool {
        return level.removePeg(pegToRemove: pegToRemove) != nil
    }

    func updatePegLocation(pegToUpdate: Peg, newLocation: LevelEntityLocation) {
        if level.pegList.contains(pegToUpdate) {
            _ = level.removePeg(pegToRemove: pegToUpdate)

            let newPeg = Peg(pegLocation: newLocation, pegColor: pegToUpdate.pegColor, pegShape: pegToUpdate.pegShape)
            _ = level.addPeg(pegToAdd: newPeg)
        }
    }

    func removeAllPegs() {
        level.removeAllPegs()
    }

    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }

    func saveLevelToFile(fileName: String) {
        let jsonString = level.encodeAsJson()
        let filePath = getDocumentsDirectory().appendingPathComponent(fileName + ".json")
        print (jsonString)
        do {
            try jsonString.write(to: filePath, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            return
        }
    }

    func loadFromFile(fileName: String) {
        let filePath = getDocumentsDirectory().appendingPathComponent(fileName + ".json")

        do {
            let jsonString = try String(contentsOf: filePath, encoding: .utf8)
            let decoder = JSONDecoder()
            level = try decoder.decode(Level.self, from: jsonString.data(using: .utf8)!)
        } catch {
            return
        }
    }

    func listLevelsToLoad() -> [URL] {
        do {
            let documentDirectoryUrl = getDocumentsDirectory()
            let files = try FileManager.default.contentsOfDirectory(
                at: documentDirectoryUrl, includingPropertiesForKeys: nil)

            var jsonFiles = [URL]()

            for file in files {
                if !file.absoluteString.contains(".json") {
                    continue
                }

                if file.absoluteString.split(separator: ".")[1] == "json" {
                    jsonFiles.append(file)
                }
            }

            return jsonFiles
        } catch {
            return [URL]()
        }
    }
}
