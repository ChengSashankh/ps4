//
//  PegLocation.swift
//  peggles
//
//  Created by Sashankh Chengavalli Kumar on 28.01.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import Foundation

class LevelEntityLocation: Equatable & Hashable & Codable {
    var xLocation: Double
    var yLocation: Double

    private enum CodingKeys: String, CodingKey {
        case xLocation
        case yLocation
    }

    init(xLocation: Double, yLocation: Double) {
        self.xLocation = xLocation
        self.yLocation = yLocation
    }

    init(vector: Vector) {
        if (vector.dimension != 2) {
//            throw VectorError.invalidDimension
            xLocation = 0.0
            yLocation = 0.0
        }

        xLocation = vector.values[0]
        yLocation = vector.values[1]
    }

    static func == (lhs: LevelEntityLocation, rhs: LevelEntityLocation) -> Bool {
        return
            lhs.xLocation == rhs.xLocation &&
            lhs.yLocation == rhs.yLocation
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(xLocation)
        hasher.combine(yLocation)
    }
}
