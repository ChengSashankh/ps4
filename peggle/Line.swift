//
//  Line.swift
//  peggle
//
//  Created by Sashankh Chengavalli Kumar on 25.02.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import Foundation


// TODO: Do we need this class?
class Line {
    var p1: Vector
    var p2: Vector

    init(startPoint: Vector, direction: Vector) {
        self.p1 = startPoint
        self.p2 = direction
    }

    init(startPoint: Vector, endPoint: Vector) {
        self.p1 = startPoint
        self.p2 = endPoint
        self.p2.subtract(otherVector: startPoint)
        self.p2.scale(constant: 1.0/self.p2.norm())
    }

    // TODO
    func doesLineIntersect(other: Line) -> Bool {
        return false
    }

    // TODO
    func doesLinePassThrough(point: Vector) -> Bool {
        return false
    }
}
