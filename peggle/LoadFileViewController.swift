//
//  LoadFileViewController.swift
//  peggles
//
//  Created by Sashankh Chengavalli Kumar on 03.02.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import UIKit

class LoadFileViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var loadFilePicker: UIPickerView!
    @IBOutlet weak var loadButton: UIButton!

    var dataToShow: [String] = [String]()
    var levelNames: [URL] = [URL]()
    var createLevelViewController: CreateLevelViewController?
    var gamePlayViewController: GamePlayViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadFilePicker.delegate = self
        self.loadFilePicker.dataSource = self

        for level in levelNames {
            dataToShow.append(getShortFileName(absoluteURLString: level.absoluteString))
        }
    }

    override func didReceiveMemoryWarning() {
       super.didReceiveMemoryWarning()
    }

    func getShortFileName(absoluteURLString: String) -> String {
        let urlComponents = absoluteURLString.components(separatedBy: "/")
        let withoutExtension = urlComponents[urlComponents.count - 1].components(separatedBy: ".")[0]
        return withoutExtension
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
       return 1
    }

    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       return dataToShow.count
    }

    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       return dataToShow[row]
    }

    @IBAction func onLoadButtonClick(_ sender: Any) {
        let selectedFile = dataToShow[loadFilePicker.selectedRow(inComponent: 0)]

        if createLevelViewController != nil {
            self.createLevelViewController!.loadLevel(fileName: selectedFile)
        } else {
            self.gamePlayViewController!.loadLevel(fileName: selectedFile)
        }
        self.dismiss(animated: true)
    }
}
