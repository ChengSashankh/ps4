//
//  Particle.swift
//  peggle-physics-engine
//
//  Created by Sashankh Chengavalli Kumar on 07.02.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import Foundation

enum BoundingType {
    case RECTANGULAR
    case RADIUS
    case TRIANGULAR
}

class Particle<T: PhysicsEngineTrackableObject>: Equatable & Hashable {
    let dimension: Int

    var mass: Double
    var position: Vector
    var velocity: Vector
    var acceleration: Vector
    var movable: Bool

    var boundingMethod: BoundingType = BoundingType.RADIUS
    var boundingRadius: Double = 10.0
    var boundingPoints: [Vector] = [Vector]()

    var payload: T? = nil

    init(dim: Int, boundingMethod: BoundingType, payload: T) {
        mass = 0
        dimension = dim
        position = Vector(dimension: dimension)
        velocity = Vector(dimension: dimension)
        acceleration = Vector(dimension: dimension)
        self.boundingMethod = boundingMethod
        movable = true
        self.payload = payload
    }

    init(
        dim: Int, mass: Double, initialPosition: Vector, initialVelocity: Vector, initialAcceleration: Vector,
        movable: Bool, payload: T) {
        self.dimension = dim
        self.mass = mass

        self.position = Vector(dimension: dim)
        self.velocity = Vector(dimension: dim)
        self.acceleration = Vector(dimension: dim)
        self.movable = movable
        self.payload = payload

        if initialPosition.dimension != self.dimension || initialVelocity.dimension != self.dimension || initialAcceleration.dimension != self.dimension {
            self.position = Vector(dimension: self.dimension)
            self.velocity = Vector(dimension: self.dimension)
            self.acceleration = Vector(dimension: self.dimension)
        } else {
            self.position = initialPosition
            self.velocity = initialVelocity
            self.acceleration = initialAcceleration
        }
    }

    static func == (lhs: Particle, rhs: Particle) -> Bool {
        return
            lhs.dimension == rhs.dimension &&
            lhs.mass == rhs.mass &&
            lhs.position == rhs.position &&
            lhs.velocity == rhs.velocity &&
            lhs.acceleration == rhs.acceleration &&
            lhs.boundingMethod == rhs.boundingMethod &&
            lhs.boundingRadius == rhs.boundingRadius &&
            lhs.boundingPoints == rhs.boundingPoints &&
            lhs.movable == rhs.movable
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(dimension)
        hasher.combine(mass)
        hasher.combine(position)
        hasher.combine(velocity)
        hasher.combine(acceleration)
        hasher.combine(boundingMethod)
        hasher.combine(boundingRadius)
        hasher.combine(boundingPoints)
        hasher.combine(movable)
    }

    func setRadiusBounds(radius: Double) {
        self.boundingRadius = radius
    }

    func setPosition(position: Vector) throws {
        if position.dimension != self.dimension {
            throw VectorError.invalidDimension
        }
        self.position = position
    }

    func setAcceleration(acceleration: Vector) throws {
        if acceleration.dimension != self.dimension {
            throw VectorError.invalidDimension
        }
        self.acceleration = acceleration
    }

    func setVelocity(velocity: Vector) throws {
        if acceleration.dimension != self.dimension {
            throw VectorError.invalidDimension
        }
        self.velocity = velocity
    }

    func applyForce(force: Vector) {
        for dim in 0...dimension {
            acceleration.values[dim] += (force.values[dim] / mass)
        }
    }

    // TODO: Test this
    // Should return direction of nearest side
    func getNearestSideToPoint(point: Vector) -> Line {
        var sides = [Line]()

        for (index, _) in boundingPoints.enumerated() {
            sides.append(Line(startPoint: boundingPoints[index], endPoint: boundingPoints[(index + 1) % boundingPoints.count]))
        }

        var minDistance = Double.greatestFiniteMagnitude
        var closestSideIndex = -1

        assert(sides.count != 0)
        assert(sides.count == boundingPoints.count)

        for (index, side) in sides.enumerated() {
            let distance = point.distanceToLine(line: side)

            if distance < minDistance {
                closestSideIndex = index
                minDistance = distance
            }
        }

        return sides[closestSideIndex]
    }


    // TODO: Return boolean that shows actual change/lack of change in value
    func updateFor(seconds: Double) throws -> Bool {
        _ = updatePositionAfter(seconds: seconds)
        _ = updateVelocityAfter(seconds: seconds)
        return movable
    }

    func updateVelocityAfter(seconds: Double) -> Bool {
        // v = u + a t
        let newVelocity = Vector(vector: velocity)
        let accelerationCompoenent = Vector(vector: acceleration)
        accelerationCompoenent.scale(constant: seconds)

        newVelocity.add(otherVector: accelerationCompoenent)

        let hasChanged = (velocity != newVelocity)
        velocity = newVelocity

        return hasChanged
    }

    func updatePositionAfter(seconds: Double) -> Bool {
        // s = u t + 0.5 a (t^2)
        let velocityComponent = Vector(vector: velocity)
        velocityComponent.scale(constant: seconds)

        let accelerationCompoenent = Vector(vector: acceleration)
        accelerationCompoenent.scale(constant: pow(seconds, 2))
        accelerationCompoenent.scale(constant: 0.5)

        velocityComponent.add(otherVector: accelerationCompoenent)

        let hasChanged = (position != velocityComponent)
        position.add(otherVector: velocityComponent)

        return hasChanged
    }

    func getLineJoiningCenters(to: Particle<T>) -> Vector {
        let firstCenter = to.position
        let secondCenter = self.position

        let lineJoiningCenters = Vector(vector: firstCenter)
        lineJoiningCenters.subtract(otherVector: secondCenter)

        return lineJoiningCenters
    }

    func getLineJoiningCenters(from: Particle<T>) -> Vector {
        let firstCenter = self.position
        let secondCenter = from.position

        let lineJoiningCenters = Vector(vector: firstCenter)
        lineJoiningCenters.subtract(otherVector: secondCenter)

        return lineJoiningCenters
    }
}
