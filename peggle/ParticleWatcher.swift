//
//  ParticleWatcher.swift
//  peggle-physics-engine
//
//  Created by Sashankh Chengavalli Kumar on 14.02.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import Foundation


class ParticleWatcher {
    var particlesInScene: [Particle<PhysicsEngineTrackableObject>]
    var timeInterval: Double
    var zonesOfInterest: [Zone]
    var bounds: [Vector]
//    var inBoundsZone: Zone?

    init(timeInterval: Double) {
        self.particlesInScene = [Particle]()
        self.timeInterval = timeInterval
        self.zonesOfInterest = [Zone]()
        self.bounds = [Vector]()
        //self.inBoundsZone = nil
    }

    func setBounds(topLeft: Vector, topRight: Vector, bottomLeft: Vector, bottomRight: Vector) {
        bounds.append(topLeft)
        bounds.append(topRight)
        bounds.append(bottomLeft)
        bounds.append(bottomRight)
    }

    /************** Add and remove particles ******************/

    func addParticleToWorld(particleToAdd: Particle<PhysicsEngineTrackableObject>) {
        particlesInScene.append(particleToAdd)
    }

    func removeParticleFromWorld(particleToRemove: Particle<PhysicsEngineTrackableObject>) {
        if let index = particlesInScene.firstIndex(of: particleToRemove) {
            particlesInScene.remove(at: index)
        }
    }

    /**************Time step particle udpates ******************/

    func updateParticlesAfterTime(t: Double) throws -> [Particle<PhysicsEngineTrackableObject>] {
        var updatedParticles = [Particle<PhysicsEngineTrackableObject>]()

        for (index, _) in particlesInScene.enumerated() {
            particlesInScene[index] = calculateEdgeRebound(particle: particlesInScene[index])

            let updated = try particlesInScene[index].updateFor(seconds: t)

            if updated {
                updatedParticles.append(particlesInScene[index])
            }
        }

        return updatedParticles
    }

    /************** Find and handle collisions ******************/

    func getCollisions() throws -> Set<Set<Particle<PhysicsEngineTrackableObject>>> {
        var collisions = Set<Set<Particle<PhysicsEngineTrackableObject>>>()

        for firstParticle in particlesInScene {
            var currentCollisions = Set<Particle<PhysicsEngineTrackableObject>>()

            for secondParticle in particlesInScene {
                if secondParticle == firstParticle {
                    continue
                }

                let particlesDoCollide = try checkIfParticlesCollide(firstParticle, secondParticle)

                if particlesDoCollide {
                    currentCollisions.insert(secondParticle)
                }
            }

            if currentCollisions.count != 0 {
                currentCollisions.insert(firstParticle)
                collisions.insert(currentCollisions)
            }
        }

        return collisions
    }

    /************** Checking collision of two objects *******************/

    func checkIfParticlesCollide(_ firstParticle: Particle<PhysicsEngineTrackableObject>, _ secondParticle: Particle<PhysicsEngineTrackableObject>) throws -> Bool {

        if firstParticle.payload is Peg && secondParticle.payload is Peg {
            return false
        }
        // Check if collision exists
        var hasIntersection = false

        switch [firstParticle.boundingMethod, secondParticle.boundingMethod] {
            case [BoundingType.RADIUS, BoundingType.RADIUS]: hasIntersection = try intersectionOfCircleAndCircle(first: firstParticle, second: secondParticle)
            case [BoundingType.RADIUS, BoundingType.TRIANGULAR], [BoundingType.TRIANGULAR, BoundingType.RADIUS]: try hasIntersection = intersectionOfCircleAndCircle(first: firstParticle, second: secondParticle)
            case [BoundingType.TRIANGULAR, BoundingType.TRIANGULAR]:
                try hasIntersection = intersectionOfCircleAndCircle(first: firstParticle, second: secondParticle)
            default: hasIntersection = false
        }

        return hasIntersection
    }

    func intersectionOfCircleAndCircle(first: Particle<PhysicsEngineTrackableObject>, second: Particle<PhysicsEngineTrackableObject>) throws -> Bool {
        let distanceBetweenParticles: Double = try first.position.distanceTo(otherVector: second.position)

        return distanceBetweenParticles <= first.boundingRadius + second.boundingRadius
    }

    /************** Checking bounds of an object *******************/

    func calculateEdgeRebound(particle: Particle<PhysicsEngineTrackableObject>) -> Particle<PhysicsEngineTrackableObject> {

        if !particle.movable {
            return particle
        }

        let position = particle.position
        let x = position.values[0]
        let y = position.values[1]
        let r = particle.boundingRadius

        if bounds.count < 4 {
            return particle
        }

        let topWallCollision    = (y - r <= bounds[0].values[1])
        let bottomWallCollision = (y + r >= bounds[2].values[1])
        let leftWallCollision   = (x - r <= bounds[0].values[0])
        let rightWallCollision  = (x + r >= bounds[1].values[0])

        var velX = particle.velocity.values[0]
        var velY = particle.velocity.values[1]

        if topWallCollision || bottomWallCollision {
            velY = -velY
        }

        if leftWallCollision || rightWallCollision {
            velX = -velX
        }

        particle.velocity = Vector(values: [velX, velY])

        return particle
    }

    /************** Check if particle has entered zones of interest *******************/

    func getParticlesInZones() -> [Particle<PhysicsEngineTrackableObject>: [Zone]] {
        var particlesInZones = [Particle<PhysicsEngineTrackableObject>: [Zone]]()

        for particle in particlesInScene {
            let zoneIndices = getZonesOccupiedByParticle(particle: particle)

            if zoneIndices.count > 0 {
                let zones = zoneIndices.map { zonesOfInterest[$0] }
                particlesInZones[particle] = zones
            }
        }

        return particlesInZones
    }

    func getZonesOccupiedByParticle(particle: Particle<PhysicsEngineTrackableObject>) -> [Int] {
        var inZoneIndices = [Int]()

        for (index, zone) in zonesOfInterest.enumerated() {
            // Check if particle is in zone of interest
            var inZone = false

            if isParticleDefinitelyNotInZone(particle: particle, zone: zone) {
                continue
            }

            // Do fine search here
            inZone = isParticleInZone(particle: particle, zone: zone)

            if inZone {
                inZoneIndices.append(index)
            }
        }

        return inZoneIndices
    }

    func isParticleDefinitelyNotInZone(particle: Particle<PhysicsEngineTrackableObject>, zone: Zone) -> Bool {
        let position = particle.position
        let x = position.values[0]
        let y = position.values[1]

        // Eliminate obviously outside particles
        let xValues = zone.vertices.map { $0.values[0] }
        let yValues = zone.vertices.map { $0.values[1] }

        assert(xValues.count > 0)
        assert(yValues.count > 0)

        let xMin: Double = xValues.min()!
        let yMin: Double = yValues.min()!
        let xMax: Double = xValues.max()!
        let yMax: Double = yValues.max()!

        if x < xMin || x > xMax || y < yMin || y > yMax {
            return true
        }

        return false
    }

    // TODO: Test this method
    func isParticleInZone(particle: Particle<PhysicsEngineTrackableObject>, zone: Zone) -> Bool {
        let positionVector = particle.position
        let x = positionVector.values[0]

        // Draw horizontal line to the right to infinity
        let pointRightInfinity = Vector(values: [x, .greatestFiniteMagnitude])
        let lineToInfinity = positionVector.getLineTo(to: pointRightInfinity)

        // Count the number of sides of the zone it intersects
        var sidesIntersected = 0
        for side in zone.sides {
            if side.doesLineIntersect(other: lineToInfinity) {
                sidesIntersected += 1
            }
        }

        // If it is odd, say it is inside. Else no.
        if sidesIntersected % 2 == 1 {
            return true
        }

        return false
    }
}
