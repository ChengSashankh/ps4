//
//  Peg.swift
//  peggles
//
//  Created by Sashankh Chengavalli Kumar on 28.01.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import Foundation

class Peg: Codable & PhysicsEngineTrackableObject {
    var pegLocation: LevelEntityLocation
    let pegColor: String
    let pegShape: String

    private enum CodingKeys: String, CodingKey {
        case pegLocation
        case pegColor
        case pegShape
    }

    init(pegLocation: LevelEntityLocation, pegColor: String, pegShape: String) {
        self.pegLocation = pegLocation
        self.pegColor = pegColor
        self.pegShape = pegShape
        super.init(id: 0)
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pegLocation = try values.decode(LevelEntityLocation.self, forKey: .pegLocation)
        pegColor = try values.decode(String.self, forKey: .pegColor)
        pegShape = try values.decode(String.self, forKey: .pegShape)
        super.init(id: 0)
    }

    static func == (lhs: Peg, rhs: Peg) -> Bool {
        return
            lhs.pegColor == rhs.pegColor &&
            lhs.id == rhs.id &&
            lhs.pegShape == rhs.pegShape
    }

    override func hash(into hasher: inout Hasher) {
        hasher.combine(pegColor)
        hasher.combine(id)
        hasher.combine(pegShape)
    }

    override func setLocation(vector: Vector) throws {
        pegLocation = LevelEntityLocation(vector: vector)
    }
}
