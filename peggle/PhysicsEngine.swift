//
//  PhysicsEngine.swift
//  peggle-physics-engine
//
//  Created by Sashankh Chengavalli Kumar on 13.02.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import Foundation
import CoreGraphics

class PhysicsEngine<T: PhysicsEngineTrackableObject> {
    var particleWatcher: ParticleWatcher = ParticleWatcher(timeInterval: (1/60.0))

    // Set the bounds of the area initially
    func setBounds(bounds: [Vector]) {
        if bounds.count == 4 {
            particleWatcher.setBounds(
                topLeft: bounds[0], topRight: bounds[1], bottomLeft: bounds[2], bottomRight: bounds[3]
            )
        }
    }

    // Add a particle to the world
    func addParticleToWorld(particle: Particle<T>) {
        particleWatcher.addParticleToWorld(particleToAdd: particle as! Particle<PhysicsEngineTrackableObject>)
    }

    // Remove a particle from the world
    func removeParticleFromWorld(particle: Particle<T>) {
        particleWatcher.removeParticleFromWorld(particleToRemove: particle as! Particle<PhysicsEngineTrackableObject>)
    }

    // Trigger motion update after a time step
    func updateAfter(seconds: Double) throws -> [Particle<T>] {
        let updatedParticles = try particleWatcher.updateParticlesAfterTime(t: seconds)
        return updatedParticles as! [Particle<T>]
    }

    // Get collisions happening right now
    func getCollisions() throws -> Set<Set<Particle<T>>> {
        return try particleWatcher.getCollisions() as! Set<Set<Particle<T>>>
    }

    // Handle the collisions happening right now
    func handleCollisions(collisions: Set<Set<Particle<T>>>) -> Set<Set<Particle<T>>> {
        var set = Set<Set<Particle<T>>>()

        for collision in collisions {
            set.insert(getCollisionResult(collision: collision))
        }

        return set
    }

    // Get objects in zones of interest
    func getObjectsInZonesOfInterest() -> [Particle<PhysicsEngineTrackableObject>: [Zone]] {
        return particleWatcher.getParticlesInZones()
    }

    // Collision handling helpers from here

    // TODO: Write more general case for this function
    // Precondition: Single moving particle
    func getCollisionResult(collision: Set<Particle<T>>) -> Set<Particle<T>> {
        // Single moving particle collision
        let fixed = collision.filter{ !$0.movable }
        var moving = collision.filter{ $0.movable }

        // TODO: Fix this
        if moving.count > 1 {
            moving = [moving.first!]
        }

        for movingParticle in moving {
            for fixedParticle in fixed {
                reflectParticleOffParticle(first: movingParticle, second: fixedParticle)
            }
        }

        return collision
    }

    // Wrapper for collisions
    func reflectParticleOffParticle(first: Particle<T>, second: Particle<T>) {
        let firstBoundType = first.boundingMethod
        let secondBoundType = second.boundingMethod

        switch [firstBoundType, secondBoundType] {
            case [BoundingType.RADIUS, BoundingType.RADIUS]:
                if first.movable {
                    CollisionHandler<T>.reflectCircleOffCircle(moving: first, fixed: second)
                } else {
                    CollisionHandler<T>.reflectCircleOffCircle(moving: second, fixed: first)
                }
            case [BoundingType.RADIUS, BoundingType.TRIANGULAR], [BoundingType.TRIANGULAR, BoundingType.RADIUS]:
                if first.boundingMethod == BoundingType.TRIANGULAR {
                    CollisionHandler<T>.reflectCircleOffTriangle(circle: second, triangle: first)
                } else {
                    CollisionHandler<T>.reflectCircleOffTriangle(circle: first, triangle: second)
                }
            default:
                print("Unexpected collision type. No action taken.")
        }
    }

    func getTriangleVerticlesFromPos(pos: CGPoint) -> [Vector] {
        let pegWidth = 20.0
        let pegHeight = 20.0

        let x = Double(pos.x)
        let y = Double(pos.y)

        var vertices = [Vector]()

        vertices.append(
            Vector(values: [x, y - pegHeight/2])
        )

        vertices.append(
            Vector(values: [x - pegWidth/2, y + pegHeight/2])
        )

        vertices.append(
            Vector(values: [x + pegWidth/2, y + pegHeight/2])
        )

        return vertices
    }
}
