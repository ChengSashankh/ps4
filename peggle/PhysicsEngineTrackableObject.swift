//
//  PhysicsEngineTrackableObject.swift
//  peggle-physics-engine
//
//  Created by Sashankh Chengavalli Kumar on 15.02.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import Foundation

class PhysicsEngineTrackableObject: Equatable & Hashable {
    var id: Int

    init(id: Int) {
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    static func == (lhs: PhysicsEngineTrackableObject, rhs: PhysicsEngineTrackableObject) -> Bool {
        return
            lhs.id == rhs.id
    }

    func setLocation(vector: Vector) throws {
        // Will be overriden in subclasses
    }
}
