//
//  SampleGameLoader.swift
//  peggle-physics-engine
//
//  Created by Sashankh Chengavalli Kumar on 16.02.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import Foundation

class SampleGameLoader {
    let firstGame = """
    {
      "validColors" : [
        "Red",
        "Blue",
        "Green"
      ],
      "lastUpdated" : 604928625.35939598,
      "name" : "testing-1",
      "pegList" : [
        {
          "pegShape" : "Circle",
          "pegColor" : "Red",
          "pegLocation" : {
            "xLocation" : 131,
            "yLocation" : 237
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Red",
          "pegLocation" : {
            "xLocation" : 214.5,
            "yLocation" : 235
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 294,
            "yLocation" : 235.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 382,
            "yLocation" : 232
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 465,
            "yLocation" : 231
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 550,
            "yLocation" : 231.5
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 183,
            "yLocation" : 292.5
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 265.5,
            "yLocation" : 289.5
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 351,
            "yLocation" : 286.5
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 427,
            "yLocation" : 285.5
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 517,
            "yLocation" : 285.5
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 618.5,
            "yLocation" : 288
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Red",
          "pegLocation" : {
            "xLocation" : 230.5,
            "yLocation" : 356.5
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Red",
          "pegLocation" : {
            "xLocation" : 326.5,
            "yLocation" : 357
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Red",
          "pegLocation" : {
            "xLocation" : 420.5,
            "yLocation" : 360.5
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Red",
          "pegLocation" : {
            "xLocation" : 520.5,
            "yLocation" : 351.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 229.5,
            "yLocation" : 325
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 313,
            "yLocation" : 318
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 339,
            "yLocation" : 183.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 163,
            "yLocation" : 165.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 263,
            "yLocation" : 177.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 445,
            "yLocation" : 171.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 551.5,
            "yLocation" : 166.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 643.5,
            "yLocation" : 173.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 658.5,
            "yLocation" : 254.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 692,
            "yLocation" : 204
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 91,
            "yLocation" : 299.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 136,
            "yLocation" : 368
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 254.5,
            "yLocation" : 418.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 403,
            "yLocation" : 423
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 524.5,
            "yLocation" : 425
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 639,
            "yLocation" : 413
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 601,
            "yLocation" : 356.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 696.5,
            "yLocation" : 347.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 734.5,
            "yLocation" : 284.5
          }
        }
      ],
      "pegLocations" : [
        {
          "xLocation" : 696.5,
          "yLocation" : 347.5
        },
        {
          "xLocation" : 420.5,
          "yLocation" : 360.5
        },
        {
          "xLocation" : 445,
          "yLocation" : 171.5
        },
        {
          "xLocation" : 639,
          "yLocation" : 413
        },
        {
          "xLocation" : 734.5,
          "yLocation" : 284.5
        },
        {
          "xLocation" : 183,
          "yLocation" : 292.5
        },
        {
          "xLocation" : 643.5,
          "yLocation" : 173.5
        },
        {
          "xLocation" : 163,
          "yLocation" : 165.5
        },
        {
          "xLocation" : 351,
          "yLocation" : 286.5
        },
        {
          "xLocation" : 136,
          "yLocation" : 368
        },
        {
          "xLocation" : 339,
          "yLocation" : 183.5
        },
        {
          "xLocation" : 294,
          "yLocation" : 235.5
        },
        {
          "xLocation" : 263,
          "yLocation" : 177.5
        },
        {
          "xLocation" : 658.5,
          "yLocation" : 254.5
        },
        {
          "xLocation" : 214.5,
          "yLocation" : 235
        },
        {
          "xLocation" : 131,
          "yLocation" : 237
        },
        {
          "xLocation" : 91,
          "yLocation" : 299.5
        },
        {
          "xLocation" : 403,
          "yLocation" : 423
        },
        {
          "xLocation" : 382,
          "yLocation" : 232
        },
        {
          "xLocation" : 465,
          "yLocation" : 231
        },
        {
          "xLocation" : 520.5,
          "yLocation" : 351.5
        },
        {
          "xLocation" : 551.5,
          "yLocation" : 166.5
        },
        {
          "xLocation" : 230.5,
          "yLocation" : 356.5
        },
        {
          "xLocation" : 427,
          "yLocation" : 285.5
        },
        {
          "xLocation" : 229.5,
          "yLocation" : 325
        },
        {
          "xLocation" : 313,
          "yLocation" : 318
        },
        {
          "xLocation" : 550,
          "yLocation" : 231.5
        },
        {
          "xLocation" : 254.5,
          "yLocation" : 418.5
        },
        {
          "xLocation" : 524.5,
          "yLocation" : 425
        },
        {
          "xLocation" : 601,
          "yLocation" : 356.5
        },
        {
          "xLocation" : 265.5,
          "yLocation" : 289.5
        },
        {
          "xLocation" : 692,
          "yLocation" : 204
        },
        {
          "xLocation" : 618.5,
          "yLocation" : 288
        },
        {
          "xLocation" : 517,
          "yLocation" : 285.5
        },
        {
          "xLocation" : 326.5,
          "yLocation" : 357
        }
      ]
    }
    """

    let secondGame = """
    {
      "validColors" : [
        "Red",
        "Blue",
        "Green"
      ],
      "lastUpdated" : 604941344.72144103,
      "name" : "preloaded-1",
      "pegList" : [
        {
          "pegShape" : "Circle",
          "pegColor" : "Red",
          "pegLocation" : {
            "xLocation" : 187,
            "yLocation" : 109.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Red",
          "pegLocation" : {
            "xLocation" : 291.5,
            "yLocation" : 119.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Red",
          "pegLocation" : {
            "xLocation" : 491.5,
            "yLocation" : 113
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 231.5,
            "yLocation" : 180.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 314.5,
            "yLocation" : 180
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 379.5,
            "yLocation" : 180
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 464.5,
            "yLocation" : 180
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 569.5,
            "yLocation" : 181
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 170.5,
            "yLocation" : 258
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 286.5,
            "yLocation" : 265
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 402,
            "yLocation" : 271
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 467,
            "yLocation" : 271
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 531.5,
            "yLocation" : 271
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 592,
            "yLocation" : 271
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 646.5,
            "yLocation" : 268.5
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 223.5,
            "yLocation" : 286
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 361,
            "yLocation" : 287
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 534,
            "yLocation" : 211
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 377,
            "yLocation" : 125
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Red",
          "pegLocation" : {
            "xLocation" : 168,
            "yLocation" : 197
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Red",
          "pegLocation" : {
            "xLocation" : 646,
            "yLocation" : 192.5
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Red",
          "pegLocation" : {
            "xLocation" : 594.5,
            "yLocation" : 121
          }
        }
      ],
      "pegLocations" : [
        {
          "xLocation" : 223.5,
          "yLocation" : 286
        },
        {
          "xLocation" : 569.5,
          "yLocation" : 181
        },
        {
          "xLocation" : 314.5,
          "yLocation" : 180
        },
        {
          "xLocation" : 379.5,
          "yLocation" : 180
        },
        {
          "xLocation" : 531.5,
          "yLocation" : 271
        },
        {
          "xLocation" : 286.5,
          "yLocation" : 265
        },
        {
          "xLocation" : 592,
          "yLocation" : 271
        },
        {
          "xLocation" : 594.5,
          "yLocation" : 121
        },
        {
          "xLocation" : 467,
          "yLocation" : 271
        },
        {
          "xLocation" : 534,
          "yLocation" : 211
        },
        {
          "xLocation" : 168,
          "yLocation" : 197
        },
        {
          "xLocation" : 170.5,
          "yLocation" : 258
        },
        {
          "xLocation" : 402,
          "yLocation" : 271
        },
        {
          "xLocation" : 646,
          "yLocation" : 192.5
        },
        {
          "xLocation" : 187,
          "yLocation" : 109.5
        },
        {
          "xLocation" : 231.5,
          "yLocation" : 180.5
        },
        {
          "xLocation" : 491.5,
          "yLocation" : 113
        },
        {
          "xLocation" : 464.5,
          "yLocation" : 180
        },
        {
          "xLocation" : 291.5,
          "yLocation" : 119.5
        },
        {
          "xLocation" : 377,
          "yLocation" : 125
        },
        {
          "xLocation" : 361,
          "yLocation" : 287
        },
        {
          "xLocation" : 646.5,
          "yLocation" : 268.5
        }
      ]
    }
    """
    let thirdGame = """
    {
      "validColors" : [
        "Red",
        "Blue",
        "Green"
      ],
      "lastUpdated" : 604941453.86687696,
      "name" : "preloaded-3",
      "pegList" : [
        {
          "pegShape" : "Circle",
          "pegColor" : "Red",
          "pegLocation" : {
            "xLocation" : 451,
            "yLocation" : 125.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 247,
            "yLocation" : 121.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 366,
            "yLocation" : 127
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 543,
            "yLocation" : 135
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 635.5,
            "yLocation" : 131.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 86,
            "yLocation" : 128
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 711.5,
            "yLocation" : 123.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 118.5,
            "yLocation" : 223.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 193,
            "yLocation" : 223
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 324.5,
            "yLocation" : 229
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 438.5,
            "yLocation" : 235
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 600,
            "yLocation" : 243.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 720,
            "yLocation" : 234.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 516,
            "yLocation" : 258.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Green",
          "pegLocation" : {
            "xLocation" : 231,
            "yLocation" : 402.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 192.5,
            "yLocation" : 320.5
          }
        },
        {
          "pegShape" : "Circle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 325,
            "yLocation" : 320.5
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 485.5,
            "yLocation" : 340
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 640,
            "yLocation" : 340
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 574.5,
            "yLocation" : 304
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 266,
            "yLocation" : 285.5
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 259,
            "yLocation" : 163
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 482.5,
            "yLocation" : 209
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 692.5,
            "yLocation" : 160.5
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 605.5,
            "yLocation" : 201.5
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 355.5,
            "yLocation" : 184.5
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 135,
            "yLocation" : 296.5
          }
        },
        {
          "pegShape" : "Triangle",
          "pegColor" : "Blue",
          "pegLocation" : {
            "xLocation" : 698.5,
            "yLocation" : 305.5
          }
        }
      ],
      "pegLocations" : [
        {
          "xLocation" : 692.5,
          "yLocation" : 160.5
        },
        {
          "xLocation" : 451,
          "yLocation" : 125.5
        },
        {
          "xLocation" : 247,
          "yLocation" : 121.5
        },
        {
          "xLocation" : 574.5,
          "yLocation" : 304
        },
        {
          "xLocation" : 600,
          "yLocation" : 243.5
        },
        {
          "xLocation" : 438.5,
          "yLocation" : 235
        },
        {
          "xLocation" : 118.5,
          "yLocation" : 223.5
        },
        {
          "xLocation" : 543,
          "yLocation" : 135
        },
        {
          "xLocation" : 366,
          "yLocation" : 127
        },
        {
          "xLocation" : 720,
          "yLocation" : 234.5
        },
        {
          "xLocation" : 485.5,
          "yLocation" : 340
        },
        {
          "xLocation" : 635.5,
          "yLocation" : 131.5
        },
        {
          "xLocation" : 259,
          "yLocation" : 163
        },
        {
          "xLocation" : 192.5,
          "yLocation" : 320.5
        },
        {
          "xLocation" : 516,
          "yLocation" : 258.5
        },
        {
          "xLocation" : 605.5,
          "yLocation" : 201.5
        },
        {
          "xLocation" : 355.5,
          "yLocation" : 184.5
        },
        {
          "xLocation" : 698.5,
          "yLocation" : 305.5
        },
        {
          "xLocation" : 86,
          "yLocation" : 128
        },
        {
          "xLocation" : 193,
          "yLocation" : 223
        },
        {
          "xLocation" : 325,
          "yLocation" : 320.5
        },
        {
          "xLocation" : 266,
          "yLocation" : 285.5
        },
        {
          "xLocation" : 711.5,
          "yLocation" : 123.5
        },
        {
          "xLocation" : 135,
          "yLocation" : 296.5
        },
        {
          "xLocation" : 324.5,
          "yLocation" : 229
        },
        {
          "xLocation" : 640,
          "yLocation" : 340
        },
        {
          "xLocation" : 482.5,
          "yLocation" : 209
        },
        {
          "xLocation" : 176.5,
          "yLocation" : 125.5
        }
      ]
    }
    """

    func getSampleLevel() -> Level? {
        do {
            let decoder = JSONDecoder()
            let level = try decoder.decode(Level.self, from: firstGame.data(using: .utf8)!)
            return level
        } catch {
            return nil
        }
    }
}
