//
//  Vector.swift
//  peggle-physics-engine
//
//  Created by Sashankh Chengavalli Kumar on 06.02.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import Foundation
import CoreGraphics

enum VectorError: Error {
    case invalidDimension
}

class Vector: Equatable & Hashable {
    var dimension: Int
    var values: [Double]

    init(dimension: Int) {
        self.dimension = dimension
        self.values = [Double](repeating: 0.0, count: self.dimension)
    }

    init(values: [Double]) {
        self.values = values
        self.dimension = values.count
    }

    init(vector: Vector) {
        self.values = vector.values
        self.dimension = vector.dimension
    }

    init(point: CGPoint) {
        self.values = [
            Double(point.x),
            Double(point.y)
        ]
        self.dimension = 2
    }

    static func == (lhs: Vector, rhs: Vector) -> Bool {
        return
            lhs.dimension == rhs.dimension &&
            lhs.values == rhs.values
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(dimension)
        hasher.combine(values)
    }

    func norm() -> Double {
        let squaredSum = values
            .map({ (value) -> Double in
                return pow(value, 2.0)
            })
            .reduce(0, +)

        return sqrt(squaredSum)
    }

    func scale(constant: Double) {
        values = values.map({ (component) -> Double in
            return component * constant
        })
    }

    func distanceTo(otherVector: Vector) throws -> Double {
        var distance: Double = 0.0

        if self.dimension != otherVector.dimension {
            throw VectorError.invalidDimension
        }

        distance =
            self.values.enumerated()
                .map({ (index, component) -> Double in
                    return pow(component - otherVector.values[index], 2.0)
                })
                .reduce(0, +)

        return pow(distance, 0.5)
    }

    func add(otherVector: Vector) {
        self.values = self.values.enumerated()
            .map { (index: Int, component: Double) -> Double in
                return component + otherVector.values[index]
            }
    }

    func subtract(otherVector: Vector) {
        self.values = self.values.enumerated()
            .map { (index: Int, component: Double) -> Double in
                return component - otherVector.values[index]
            }
    }

    func multiplyComponents(otherVector: Vector) {
        self.values = self.values.enumerated()
            .map { (index: Int, component: Double) -> Double in
                return component * otherVector.values[index]
            }
    }

    func dot(otherVector: Vector) -> Double {
        return self.values.enumerated()
            .map { (offset: Int, element: Double) -> Double in
                return element * otherVector.values[offset]
            }
            .reduce(0, +)
    }

    func cosOfAngleWith(otherVector: Vector) -> Double {
        return self.dot(otherVector: otherVector) / (self.norm() * otherVector.norm())
    }

    func sinOfAngleWith(otherVector: Vector) -> Double {
        let magnitude = values[0] * otherVector.values[1] - otherVector.values[0] * values[1]
        return magnitude / (self.norm() * otherVector.norm())
    }

    func getComponentAlong(otherVector: Vector) -> Vector {
        let magnitude = dot(otherVector: otherVector)

        let direction = Vector(vector: otherVector)
        direction.scale(constant: magnitude/otherVector.norm())

        return direction
    }

    func getComponentPerpendicular(otherVector: Vector) -> Vector {
        let sinValue = sinOfAngleWith(otherVector: otherVector)
        let direction = otherVector.getPerpendicularDirection()

        let magnitude = sinValue * self.norm()
        direction.scale(constant: magnitude)
        return direction
    }

    // TODO: Test that this is a unit vector
    func getPerpendicularDirection() -> Vector {
        let perpendicularDirection = Vector(values: [-1 * self.values[1], self.values[0]])
        perpendicularDirection.scale(constant: 1.0/perpendicularDirection.norm())
        return perpendicularDirection
    }

    func distanceToLine(line: Line) -> Double {
        let lineDirection = Vector(vector: line.p2)
        lineDirection.subtract(otherVector: line.p1)

        let positionVectorOfPoint = Vector(vector: self)
        positionVectorOfPoint.subtract(otherVector: line.p1)

        let sineValue = lineDirection.sinOfAngleWith(otherVector: positionVectorOfPoint)

        return sineValue * positionVectorOfPoint.norm()
    }

    func getLineTo(to: Vector) -> Line {
        return Line(startPoint: self, endPoint: to)
    }
}
