//
//  Zone.swift
//  peggle
//
//  Created by Sashankh Chengavalli Kumar on 28.02.20.
//  Copyright © 2020 Sashankh Chengavalli Kumar. All rights reserved.
//

import Foundation

class Zone {
    var sides: [Line]
    var vertices: [Vector]
    var zoneName: String

    init(sides: [Line], zoneName: String) {
        self.sides = sides
        self.zoneName = zoneName

        var verticesDistinct = Set<Vector>()
        for side in self.sides {
            verticesDistinct.insert(side.p1)
            verticesDistinct.insert(side.p2)
        }

        vertices = Array(verticesDistinct)

        assert(sides.count == vertices.count)
    }

//    init(vertices: [Vector]) {
//        self.vertices = vertices
//
//        self.sides = [Line]()
//        let numVertices = vertices.count
//
//        for i in 0...numVertices {
//            self.sides.append(Line(startPoint: vertices[i], endPoint: vertices[(i + 1) % numVertices]))
//        }
//
//        assert(sides.count == vertices.count)
//    }
}
